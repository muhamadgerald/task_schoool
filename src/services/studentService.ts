import { Student } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";

export class StudentService {
    private static studentRepository: Repository<Student> = null;

    constructor(di: IDI) {
        if (StudentService.studentRepository === null) {
            StudentService.studentRepository = di.appTypeOrm.getConnection().getRepository(Student);
        }
    }

    public async findAll(query: PageableGetParam): Promise<Student[]> {
        return await StudentService.studentRepository.find(Pagination.paginateOption(query));
    }

    public async findById(id: number): Promise<Student> {
        try {
            return await StudentService.studentRepository
                .findOne({
                    where: {
                        id
                    }
                });
        } catch (e) {
            console.log(e);
            throw new DataNotFoundError("Invalid code");
        }
    }

    public async create(data: Student): Promise<Student> {
        const student = new Student();
        student.name = data.name;
        try {
            return await StudentService.studentRepository.save(student);
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(data: Student): Promise<Student> {
        const student: Student = await this.findById(data.id);
        if (student == null) {
            throw new DataNotFoundError(`Student not found: ${data.id}`);
        }

        if (data.name !== null || data.name !== undefined) {
            student.name = data.name;
        }

        return await StudentService.studentRepository.save(student);
    }

    public async delete(code: number): Promise<Student> {
        const student: Student = await this.findById(code);
        if (student == null) {
            throw new DataNotFoundError(`Student not found: ${code}`);
        }
        await StudentService.studentRepository.delete(student);
        return student;
    }
}