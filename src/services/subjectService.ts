import { Subject } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";

export class SubjectService {
    private static subjectRepository: Repository<Subject> = null;

    constructor(di: IDI) {
        if (SubjectService.subjectRepository === null) {
            SubjectService.subjectRepository = di.appTypeOrm.getConnection().getRepository(Subject);
        }
    }

    public async findAll(query: PageableGetParam): Promise<Subject[]> {
        return await SubjectService.subjectRepository.find(Pagination.paginateOption(query));
    }

    public async findById(id: number): Promise<Subject> {
        try {
            return await SubjectService.subjectRepository
                .findOne({
                    where: {
                        id
                    }
                });
        } catch (e) {
            console.log(e);
            throw new DataNotFoundError("Invalid code");
        }
    }

    public async create(data: Subject): Promise<Subject> {
        const subject = new Subject();
        subject.name = data.name;
        try {
            return await SubjectService.subjectRepository.save(subject);
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(data: Subject): Promise<Subject> {
        const subject: Subject = await this.findById(data.id);
        if (subject == null) {
            throw new DataNotFoundError(`Subject not found: ${data.id}`);
        }

        if (data.name !== null || data.name !== undefined) {
            subject.name = data.name;
        }

        return await SubjectService.subjectRepository.save(subject);
    }

    public async delete(code: number): Promise<Subject> {
        const subject: Subject = await this.findById(code);
        if (subject == null) {
            throw new DataNotFoundError(`Subject not found: ${code}`);
        }
        await SubjectService.subjectRepository.delete(subject);
        return subject;
    }
}