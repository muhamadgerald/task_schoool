import { SubjectScore } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";

export class SubjectScoreService {
    private static subjectScoreRepository: Repository<SubjectScore> = null;

    constructor(di: IDI) {
        if (SubjectScoreService.subjectScoreRepository === null) {
            SubjectScoreService.subjectScoreRepository = di.appTypeOrm.getConnection().getRepository(SubjectScore);
        }
    }

    public async findAll(query: PageableGetParam): Promise<SubjectScore[]> {
        return await SubjectScoreService.subjectScoreRepository.find(Pagination.paginateOption(query, {
            relations: ['classId', 'subjectId', 'studentId']
        }));
    }

    public async findById(id: number): Promise<SubjectScore> {
        try {
            return await SubjectScoreService.subjectScoreRepository
                .findOne({
                    where: {
                        id
                    }
                });
        } catch (e) {
            console.log(e);
            throw new DataNotFoundError("Invalid code");
        }
    }

    public async create(data: SubjectScore): Promise<SubjectScore> {
        const subjectScore = new SubjectScore();
        subjectScore.classId = data.classId;
        subjectScore.subjectId = data.subjectId;
        subjectScore.studentId = data.studentId;
        subjectScore.term_1 = data.term_1;
        subjectScore.term_2 = data.term_2;
        subjectScore.term_3 = data.term_3;

        try {
            return await SubjectScoreService.subjectScoreRepository.save(subjectScore);
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(data: SubjectScore): Promise<SubjectScore> {
        const subjectScore: SubjectScore = await this.findById(data.id);
        if (subjectScore == null) {
            throw new DataNotFoundError(`Subject Score not found: ${data.id}`);
        }

        if (data.classId !== null || data.classId !== undefined) {
            subjectScore.classId = data.classId;
        }
        if (data.subjectId !== null || data.subjectId !== undefined) {
            subjectScore.subjectId = data.subjectId;
        }
        if (data.studentId !== null || data.studentId !== undefined) {
            subjectScore.studentId = data.studentId;
        }
        if (data.term_1 !== null || data.term_1 !== undefined) {
            subjectScore.term_1 = data.term_1;
        }
        if (data.term_2 !== null || data.term_2 !== undefined) {
            subjectScore.term_2 = data.term_2;
        }
        if (data.term_3 !== null || data.term_3 !== undefined) {
            subjectScore.term_3 = data.term_3;
        }

        return await SubjectScoreService.subjectScoreRepository.save(subjectScore);
    }

    public async delete(code: number): Promise<SubjectScore> {
        const subjectScore: SubjectScore = await this.findById(code);
        if (subjectScore == null) {
            throw new DataNotFoundError(`Subject Score not found: ${code}`);
        }
        await SubjectScoreService.subjectScoreRepository.delete(subjectScore);
        return subjectScore;
    }
}