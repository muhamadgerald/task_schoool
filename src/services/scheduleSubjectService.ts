import { ScheduleSubject } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";

export class ScheduleSubjectService {
    private static scheduleSubjectRepository: Repository<ScheduleSubject> = null;

    constructor(di: IDI) {
        if (ScheduleSubjectService.scheduleSubjectRepository === null) {
            ScheduleSubjectService.scheduleSubjectRepository = di.appTypeOrm.getConnection().getRepository(ScheduleSubject);
        }
    }

    public async findAll(query: PageableGetParam): Promise<ScheduleSubject[]> {
        return await ScheduleSubjectService.scheduleSubjectRepository.find(Pagination.paginateOption(query));
    }

    public async create(data: ScheduleSubject): Promise<ScheduleSubject> {
        const scheduleSubject = new ScheduleSubject();
        scheduleSubject.scheduleDayId = data.scheduleDayId;
        scheduleSubject.subjectId = data.subjectId;
        scheduleSubject.teacherId = data.teacherId;
        scheduleSubject.startTime = data.startTime;
        scheduleSubject.endTime = data.endTime;
        
        try {
            return await ScheduleSubjectService.scheduleSubjectRepository.save(scheduleSubject);
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
}