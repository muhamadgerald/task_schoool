import { Schedule, ScheduleSubject } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";
import { ClassService } from "./classService";
import { TeacherService } from "./teacherService";
import { SubjectService } from "./subjectService";
import { ScheduleSubjectService } from "./scheduleSubjectService";


export class ScheduleService {
    private static scheduleRepository: Repository<Schedule> = null;
    private static classRepository: ClassService = null;
    private static teacherRepository: TeacherService = null;
    private static subjectRepository: SubjectService = null;
    private static scheduleSubject: ScheduleSubjectService = null;
    
    constructor(di: IDI) {
        if (ScheduleService.scheduleRepository === null) {
            ScheduleService.scheduleRepository = di.appTypeOrm.getConnection().getRepository(Schedule);
        }

        if (ScheduleService.classRepository === null) {
            ScheduleService.classRepository = new ClassService(di);
        }

        if (ScheduleService.teacherRepository === null) {
            ScheduleService.teacherRepository = new TeacherService(di);
        }

        if (ScheduleService.subjectRepository === null) {
            ScheduleService.subjectRepository = new SubjectService(di);
        }

        if(ScheduleService.scheduleSubject === null) {
            ScheduleService.scheduleSubject = new ScheduleSubjectService(di);
        }
    }

    public async findAll(query: PageableGetParam): Promise<Schedule[]> {
        return await ScheduleService.scheduleRepository.find(Pagination.paginateOption(query, {
            relations: ['class']
        }));
    }

    public async findById(id: number): Promise<Schedule> {
        try {
            return await ScheduleService.scheduleRepository
                .findOne({
                    where: {
                        id
                    }
                });
        } catch (e) {
            console.log(e);
            throw new DataNotFoundError("Invalid code");
        }
    }

    public async create(data: any): Promise<Schedule> {

        const checkClass = await ScheduleService.classRepository.findById(data.classId);
        if(!checkClass) {
            throw new DataNotFoundError(`Class ${data.classId} does not exist`);
        }

        const checkSubject = await ScheduleService.subjectRepository.findById(data.subjectId);
        if(!checkSubject) {
            throw new DataNotFoundError(`Subject ${data.classId} does not exist`);
        }

        const checkTeacher = await ScheduleService.teacherRepository.findById(data.teacherId);
        if (!checkTeacher){
            throw new DataNotFoundError(`Teacher ${data.classId} does not exist`);
        }

        const schedule = new Schedule();
        schedule.classId = data.classId;
        schedule.day = data.day;
        const scheduleSave = await ScheduleService.scheduleRepository.save(schedule);
        const scheduleId: any = scheduleSave.id;

        const scheduleSubject = new ScheduleSubject();
        scheduleSubject.scheduleDayId = scheduleId;
        scheduleSubject.subjectId = data.subjectId;
        scheduleSubject.teacherId = data.teacherId;
        scheduleSubject.startTime = data.startTime;
        scheduleSubject.endTime = data.endTime;
        await ScheduleService.scheduleSubject.create(scheduleSubject);
      
        try {
            return schedule;
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(data: Schedule): Promise<Schedule> {
        const schedule: Schedule = await this.findById(data.id);
        if (schedule == null) {
            throw new DataNotFoundError(`Schedule not found: ${data.id}`);
        }

        if (data.classId !== null || data.classId !== undefined) {
            schedule.classId = data.classId;
        }

        if (data.day !== null || data.day !== undefined) {
            schedule.day = data.day;
        }


        return await ScheduleService.scheduleRepository.save(schedule);
    }

    public async delete(code: number): Promise<Schedule> {
        const schedule: Schedule = await this.findById(code);
        if (schedule == null) {
            throw new DataNotFoundError(`Schedule not found: ${code}`);
        }
        await ScheduleService.scheduleRepository.delete(schedule);
        return schedule;
    }
}