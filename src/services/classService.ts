import { Class, ClassMember } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";
import { TeacherService } from "./teacherService";
import { StudentService } from "./studentService";
import { ClassMemberService } from "./classMemberService";

// type createClass = {
//     name: string,
//     homeroomTeacherId: number,
//     year: number,
//     classId: number,
//     studentId: number
// }

export class ClassService {
    private static classRepository: Repository<Class> = null;
    private static teacherRepository: TeacherService = null;
    private static studentRepository: StudentService = null;
    private static classMemberRepository: ClassMemberService = null;
    
    constructor(di: IDI) {
        if (ClassService.classRepository === null) {
            ClassService.classRepository = di.appTypeOrm.getConnection().getRepository(Class);
        }

        if (ClassService.teacherRepository === null) {
            ClassService.teacherRepository = new TeacherService(di);
        }

        if (ClassService.studentRepository === null) {
            ClassService.studentRepository = new StudentService(di);
        }

        if (ClassService.classMemberRepository === null) {
            ClassService.classMemberRepository = new ClassMemberService(di);
        }
    }

    public async findAll(query: PageableGetParam): Promise<Class[]> {
        return await ClassService.classRepository.find(Pagination.paginateOption(query));
    }

    public async findById(id: number): Promise<Class> {
        try {
            return await ClassService.classRepository
                .findOne({
                    where: {
                        id
                    }
                });
        } catch (e) {
            console.log(e);
            throw new DataNotFoundError("Invalid code");
        }
    }

    public async create(data: any): Promise<Class> {

        const checkTeacher = await ClassService.teacherRepository.findById(data.homeroomTeacherId);
        if(!checkTeacher) {
            throw new DataNotFoundError(`Teacher ${data.homeroomTeacherId} does not exist`);
        }

        for(let i = 0; i < data.students.length; i++){
            const checkStudents = await ClassService.studentRepository.findById(data.students[i].studentId);
            if(!checkStudents) {
                throw new DataNotFoundError(`Student ${data.students[i].studentId} does not exist`);
            }
        }

        const classEntity = new Class();
        classEntity.name = data.name;
        classEntity.homeroomTeacherId = data.homeroomTeacherId;
        classEntity.year = data.year;
        const classEntitySave = await ClassService.classRepository.save(classEntity);
        const classEntityId: any = classEntitySave.id;
        for(let i = 0; i < data.students.length; i++) {
            const classMember = new ClassMember();
            classMember.classID = classEntityId;
            classMember.student_id = data.students[i].studentId;
            console.log('dari class service', classMember);
            await ClassService.classMemberRepository.create(classMember);
        }
        try {
            return classEntitySave;
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(data: Class): Promise<Class> {
        const classEntity: Class = await this.findById(data.id);
        if (classEntity == null) {
            throw new DataNotFoundError(`Class not found: ${data.id}`);
        }

        if (data.name !== null || data.name !== undefined) {
            classEntity.name = data.name;
        }

        if (data.year !== null || data.year !== undefined) {
            classEntity.year = data.year;
        }

        if (data.homeroomTeacherId !== null || data.homeroomTeacherId !== undefined) {
            classEntity.homeroomTeacherId = data.homeroomTeacherId;
        }

        return await ClassService.classRepository.save(classEntity);
    }

    public async delete(code: number): Promise<Class> {
        const classEntity: Class = await this.findById(code);
        if (classEntity == null) {
            throw new DataNotFoundError(`Class not found: ${code}`);
        }
        await ClassService.classRepository.delete(classEntity);
        return classEntity;
    }
}