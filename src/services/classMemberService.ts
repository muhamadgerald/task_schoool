import { ClassMember } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";

export class ClassMemberService {
    private static classMemberRepository: Repository<ClassMember> = null;

    constructor(di: IDI) {
        if (ClassMemberService.classMemberRepository === null) {
            ClassMemberService.classMemberRepository = di.appTypeOrm.getConnection().getRepository(ClassMember);
        }
    }

    public async findAll(query: PageableGetParam): Promise<ClassMember[]> {
        return await ClassMemberService.classMemberRepository.find(Pagination.paginateOption(query));
    }

    // public async findById(id: number): Promise<ClassMember> {
    //     try {
    //         return await ClassMemberService.classMemberRepository
    //             .findOne({
    //                 where: {
    //                     id
    //                 }
    //             });
    //     } catch (e) {
    //         console.log(e);
    //         throw new DataNotFoundError("Invalid code");
    //     }
    // }

    public async create(data: ClassMember): Promise<ClassMember> {
        const classMember = new ClassMember();
        classMember.classID = data.classID;
        classMember.student_id = data.student_id;
        console.log('DARI CLASS MEMBER SERVICE', classMember);
        
        try {
            return await ClassMemberService.classMemberRepository.save(classMember);
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    // public async update(data: ClassMember): Promise<ClassMember> {
    //     const classMember: ClassMember = await this.findById(data.id);
    //     if (classMember == null) {
    //         throw new DataNotFoundError(`ClassMember not found: ${data.id}`);
    //     }

    //     if (data.classId !== null || data.classId !== undefined) {
    //         classMember.classId = data.classId;
    //     }

    //     if (data.studentId !== null || data.studentId !== undefined) {
    //         classMember.studentId = data.studentId;
    //     }

    //     return await ClassMemberService.classMemberRepository.save(classMember);
    // }

    // public async delete(code: number): Promise<ClassMember> {
    //     const classMember: ClassMember = await this.findById(code);
    //     if (classMember == null) {
    //         throw new DataNotFoundError(`ClassMember not found: ${code}`);
    //     }
    //     await ClassMemberService.classMemberRepository.delete(classMember);
    //     return classMember;
    // }
}