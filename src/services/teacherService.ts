import { Teacher } from "../entity";
import { IDI } from "../interface";
import { Repository } from "typeorm";
import { PageableGetParam } from "../lib/appValidator";
import { Pagination } from "../lib/helper/pagination";
import { DataNotFoundError } from "../error";

export class TeacherService {
    private static teacherRepository: Repository<Teacher> = null;

    constructor(di: IDI) {
        if (TeacherService.teacherRepository === null) {
            TeacherService.teacherRepository = di.appTypeOrm.getConnection().getRepository(Teacher);
        }
    }

    public async findAll(query: PageableGetParam): Promise<Teacher[]> {
        return await TeacherService.teacherRepository.find(Pagination.paginateOption(query));
    }

    public async findById(id: number): Promise<Teacher> {
        try {
            return await TeacherService.teacherRepository
                .findOne({
                    where: {
                        id
                    }
                });
        } catch (e) {
            console.log(e);
            throw new DataNotFoundError("Invalid code");
        }
    }

    public async create(data: Teacher): Promise<Teacher> {
        const teacher = new Teacher();
        teacher.name = data.name;
        try {
            return await TeacherService.teacherRepository.save(teacher);
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }

    public async update(data: Teacher): Promise<Teacher> {
        const teacher: Teacher = await this.findById(data.id);
        if (teacher == null) {
            throw new DataNotFoundError(`Teacher not found: ${data.id}`);
        }

        if (data.name !== null || data.name !== undefined) {
            teacher.name = data.name;
        }

        return await TeacherService.teacherRepository.save(teacher);
    }

    public async delete(code: number): Promise<Teacher> {
        const teacher: Teacher = await this.findById(code);
        if (teacher == null) {
            throw new DataNotFoundError(`Teacher not found: ${code}`);
        }
        await TeacherService.teacherRepository.delete(teacher);
        return teacher;
    }
}