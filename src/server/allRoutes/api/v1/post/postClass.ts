import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../interface";
import { BodyFormat, ValidatorGetParam } from "../../../../../lib";
// import {
//     commonHeaderParams,
// } from "../../../../../scripts";
import {PageableGetParam} from "../../../../../lib/appValidator";
import { Class } from "../../../../../entity";

const path = "/v1/classes";
const method = "POST";
const get: ValidatorGetParam[] = [];
const body: BodyFormat = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        name: {
            type: "string",
            required: true
        },
        homeroomTeacherId: {
            type: "number",
            required: true
        },
        year: {
            type: "number",
            required: true
        },
        students: {
            type: "array",
            required: true,
            items: {
                type: "object",
                required: true,
                fields: {
                    studentId: {
                        type: "number",
                        required: true
                    }
                }
            }
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            name: {
                type: "any"
            },
            homeroomTeacherId: {
                type: "any"
            },
            year: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat: ResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx: IRouterContext): Promise<void> => {
    const di: IDI = ctx.state.di;

    // Validate query params
    di.validator.processQuery<PageableGetParam>(get, ctx.request.query);

    // Validate body
    const requestBody = di.validator.processBody<Class>(body, ctx.request.body);

    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);

    ctx.status = 200;
    ctx.body = await di.classService.create(requestBody);
};
export const openapiPostClass: IOpenApiRoute = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
export const postClass: IAllRoute = {
    func,
    method,
    path,
};
