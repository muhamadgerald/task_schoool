import { IRouterContext } from "koa-router";
import { ResponseFormat } from "partial-responsify";
import { IAllRoute, IDI, IOpenApiRoute } from "../../../../../interface";
import { BodyFormat, ValidatorGetParam } from "../../../../../lib";
// import {
//     commonHeaderParams,
// } from "../../../../../scripts";
import {PageableGetParam} from "../../../../../lib/appValidator";
import { SubjectScore } from "../../../../../entity";

const path = "/v1/subjectscore";
const method = "POST";
const get: ValidatorGetParam[] = [];
const body: BodyFormat = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        classId: {
            type: "number",
            required: true
        },
        subjectId: {
            type: "number",
            required: true
        },
        studentId: {
            type: "number",
            required: true
        },
        term_1: {
            type: "string",
            required: true
        },
        term_2: {
            type: "string",
            required: true
        },
        term_3: {
            type: "string",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat: ResponseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            name: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat: ResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx: IRouterContext): Promise<void> => {
    const di: IDI = ctx.state.di;

    // Validate query params
    di.validator.processQuery<PageableGetParam>(get, ctx.request.query);

    // Validate body
    const requestBody = di.validator.processBody<SubjectScore>(body, ctx.request.body);

    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);

    ctx.status = 200;
    ctx.body = await di.subjectScoreService.create(requestBody);
};
export const openapiPostSubjectScore: IOpenApiRoute = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
export const postSubjectScore: IAllRoute = {
    func,
    method,
    path,
};
