import { IAllRoute, IOpenApiRoute } from "../../../../interface";

// ROUTE CONTROLER IMPORTS
// GET ROUTES
import { openapiGetSubject, getSubject } from './get/getSubject';
import { openapiGetTeacher, getTeacher } from './get/getTeacher';
import { openapiGetStudent, getStudent } from './get/getStudent';
import { openapiGetClasses, getClasses } from './get/getClass';
import { openapiGetClassMember, getClassMember } from './get/getClassMember';
import { openapiGetSchedule, getSchedule } from './get/getSchedule';
import { openapiGetScheduleSubject, getScheduleSubject } from './get/getScheduleSubject';
import { openapiGetSubjectScore, getSubjectScore } from './get/getSubjectScore';

// POST ROUTES
import { openapiPostSubject, postSubject } from './post/postSubject';
import { openapiPostTeacher, postTeacher } from './post/postTeacher';
import { openapiPostStudent, postStudent } from './post/postStudent';
import { openapiPostClass, postClass } from './post/postClass';
import { openapiPostSchedule, postSchedule } from './post/postSchedule';
import { openapiPostSubjectScore, postSubjectScore } from './post/postSubjectScore';

// PUT ROUTES
import { openapiPutSubject, putSubject } from './put/putSubject';
import { openapiPutTeacher, putTeacher } from './put/putTeacher';
import { openapiPutStudent, putStudent } from './put/putStudent';
import { openapiPutClasses, putClasses } from './put/putClass';

// DELETE ROUTES
import { openapiDeleteSubject, deleteSubject } from './delete/deleteSubject';
import { openapiDeleteTeacher, deleteTeacher } from './delete/deleteTeacher';
import { openapiDeleteStudent, deleteStudent } from './delete/deleteStudent';
import { openapiDeleteClasses, deleteClasses } from './delete/deleteClass';

interface IRouteTuple {
    0: IOpenApiRoute;
    1: IAllRoute;
}

// REGISTER THE ROUTE CONTROLLER HERE
const routes: IRouteTuple[] = [
    // [openapiGetProduct, getProduct],
    [openapiGetSubject, getSubject],
    [openapiGetTeacher, getTeacher],
    [openapiGetStudent, getStudent],
    [openapiGetClasses, getClasses],
    [openapiGetClassMember, getClassMember],
    [openapiGetSchedule, getSchedule],
    [openapiGetScheduleSubject, getScheduleSubject],
    [openapiGetSubjectScore, getSubjectScore],

    // // POST ROUTES
    [openapiPostSubject, postSubject],
    [openapiPostTeacher, postTeacher],
    [openapiPostStudent, postStudent],
    [openapiPostClass, postClass],
    [openapiPostSchedule, postSchedule],
    [openapiPostSubjectScore, postSubjectScore],

    // // PUT ROUTES
    [openapiPutSubject, putSubject],
    [openapiPutTeacher, putTeacher],
    [openapiPutStudent, putStudent],
    [openapiPutClasses, putClasses],

    // DELETE ROUTES
    // [openapiDeleteSupplier, deleteSupplier],
    [openapiDeleteSubject, deleteSubject],
    [openapiDeleteTeacher, deleteTeacher],
    [openapiDeleteStudent, deleteStudent],
    [openapiDeleteClasses, deleteClasses]
    
];
const tmpBaseOpenApiRoutes: IOpenApiRoute[] = [];
const tmpBaseRoutes: IAllRoute[] = [];
for (const route of routes) {
    if (!route[1].path.startsWith("/v1/") || !route[0].path.startsWith("/v1/")) {
        // tslint:disable-next-line: no-console
        console.log(route[1]);
        throw new Error("path should start with v1");
    }
    tmpBaseOpenApiRoutes.push(route[0]);
    tmpBaseRoutes.push(route[1]);
}

// seperate the route to openapi and application route, future easier to extend
export const baseOpenApiRoutes: IOpenApiRoute[] = tmpBaseOpenApiRoutes;
export const baseRoutes: IAllRoute[] = tmpBaseRoutes;
