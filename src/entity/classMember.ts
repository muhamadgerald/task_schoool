import { Entity, ManyToOne, JoinColumn, PrimaryColumn } from 'typeorm';
import { Class } from './class';
import { Student } from './student';

@Entity({name: 'class_member'})

export class ClassMember {
    @PrimaryColumn({name: 'class_id'})
    @ManyToOne(() => Class, classId => classId.id, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn({name: 'class_id'})
    public classID: Class;

    @PrimaryColumn({ name: 'student_id'})
    @ManyToOne(() => Student, student => student.id)
    @JoinColumn({ name: 'student_id'})
    student_id: Student;
}
