import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Schedule } from './schedule';
import { Subject } from './subject';
import { Teacher } from './teacher';

@Entity({name: 'schedule_subject'})

export class ScheduleSubject {

    @PrimaryColumn({name: 'schedule_day_id'})
    @ManyToOne(() => Schedule, schedule => schedule.id)
    @JoinColumn({name: 'schedule_day_id'})
    scheduleDayId: Schedule;

    @ManyToOne(() => Subject, subject => subject.id)
    @JoinColumn({name: 'subject_id'})
    subjectId: Subject;

    @ManyToOne(() => Teacher, teacher => teacher.id)
    @JoinColumn({name: 'teacher_id'})
    teacherId: Teacher;

    @Column({name: 'start_time'})
    startTime: Date;

    @Column({name: 'end_time'})
    endTime: Date;
}