import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Class } from './class';

@Entity({name: 'schedule'})

export class Schedule {
    @PrimaryGeneratedColumn({name: 'id'})
    id: number;

    @ManyToOne(() => Class, classId => classId.id)
    @JoinColumn({name: 'class_id'})
    classId: Class;

    @Column({name: 'day'})
    day: string;
}