import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({
    name: 'teacher'
})

export class Teacher {
    @PrimaryGeneratedColumn({name: "id"})
    public id: number;

    @Column({name: 'name'})
    public name: string;
    
}