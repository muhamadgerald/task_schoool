import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Class } from './class';
import { Student } from './student';
import { Subject } from './subject';

@Entity({name: 'subject_score'})

export class SubjectScore {
    @PrimaryGeneratedColumn({name: "id"})
    id: number;

    @ManyToOne(() => Class, classId => classId.id)
    @JoinColumn({name: 'class_id'})
    classId: number;

    @ManyToOne(() => Subject, subject => subject.id)
    @JoinColumn({name: 'subject_id'})
    subjectId: number;

    @ManyToOne(() => Student, student => student.id)
    @JoinColumn({name: 'student_id'})
    studentId: number;

    @Column({name: 'term_1'})
    term_1: string;

    @Column({name: 'term_2'})
    term_2: string;

    @Column({name: 'term_3'})
    term_3: string;
}