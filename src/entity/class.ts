import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Teacher } from './teacher';

@Entity({
    name: 'class'
})

export class Class {
    @PrimaryGeneratedColumn({name: 'id'})
    id: number;

    @Column({name: 'name'})
    name: string;

    @ManyToOne(() => Teacher, teacher => teacher.id)
    @JoinColumn({name: 'homeroom_teacher_id'})
    homeroomTeacherId: number;

    @Column({name: 'year'})
    year: number;

}
