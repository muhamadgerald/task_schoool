import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({
    name: 'student'
})

export class Student {
    @PrimaryGeneratedColumn({name: "id"})
    public id: number;

    @Column({name: 'name'})
    public name: string;
    
}