import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({
    name: 'subject'
})

export class Subject {
    @PrimaryGeneratedColumn({name: "id"})
    public id: number;

    @Column({name: 'name'})
    public name: string;
    
}