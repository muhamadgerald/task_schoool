// tslint:disable:max-line-length
import { Validator } from "le-validator";
import { PartialResponsify } from "partial-responsify";
import { Class, ClassMember, Schedule, ScheduleSubject, Student, Subject, SubjectScore, Teacher } from "../entity";
import { IDI } from "../interface";
import {
    Config,
    Helper,
    AppTypeOrm
} from "../lib";
import { ClassService, ScheduleService, ScheduleSubjectService, StudentService, SubjectScoreService, SubjectService, TeacherService } from "../services";
import { ClassMemberService } from "../services/classMemberService";

export const getAppTypeOrm = async (config: Config): Promise<AppTypeOrm> => {
    return new AppTypeOrm(await AppTypeOrm.getConnectionManager({
        database: config.dbDatabase,
        entities: [
            Subject,
            Student,
            Teacher,
            Class,
            ClassMember,
            Schedule,
            ScheduleSubject,
            SubjectScore
        ],
        host: config.dbHost,
        logging: config.dbLogging,
        password: config.dbPassword,
        type: config.dbType,
        username: config.dbUsername,
    }));
};

export const getDiComponent = async (config: Config): Promise<IDI> => {
    const di: IDI = {
        appTypeOrm: await getAppTypeOrm(config),
        config,
        helper: new Helper(),
        partialResponsify: new PartialResponsify(),
        validator: new Validator(),
        subjectService: null,
        teacherService: null,
        studentService: null,
        classService: null,
        classMemberService: null,
        scheduleService: null,
        scheduleSubjectService: null,
        subjectScoreService: null
    };

    di.subjectService = new SubjectService(di);
    di.teacherService = new TeacherService(di);
    di.studentService = new StudentService(di);
    di.classService = new ClassService(di);
    di.classMemberService = new ClassMemberService(di);
    di.scheduleService = new ScheduleService(di);
    di.scheduleSubjectService = new ScheduleSubjectService(di);
    di.subjectScoreService = new SubjectScoreService(di);
    return di;
};
