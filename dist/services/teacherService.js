"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TeacherService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class TeacherService {
    constructor(di) {
        if (TeacherService.teacherRepository === null) {
            TeacherService.teacherRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Teacher);
        }
    }
    async findAll(query) {
        return await TeacherService.teacherRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await TeacherService.teacherRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const teacher = new entity_1.Teacher();
        teacher.name = data.name;
        try {
            return await TeacherService.teacherRepository.save(teacher);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const teacher = await this.findById(data.id);
        if (teacher == null) {
            throw new error_1.DataNotFoundError(`Teacher not found: ${data.id}`);
        }
        if (data.name !== null || data.name !== undefined) {
            teacher.name = data.name;
        }
        return await TeacherService.teacherRepository.save(teacher);
    }
    async delete(code) {
        const teacher = await this.findById(code);
        if (teacher == null) {
            throw new error_1.DataNotFoundError(`Teacher not found: ${code}`);
        }
        await TeacherService.teacherRepository.delete(teacher);
        return teacher;
    }
}
exports.TeacherService = TeacherService;
TeacherService.teacherRepository = null;
//# sourceMappingURL=teacherService.js.map