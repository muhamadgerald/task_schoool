"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CountryService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const currencyService_1 = require("./currencyService");
const error_1 = require("../error");
class CountryService {
    constructor(di) {
        if (CountryService.countryRepository === null) {
            CountryService.countryRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Country);
        }
        if (CountryService.currencyService === null) {
            CountryService.currencyService = new currencyService_1.CurrencyService(di);
        }
        console.log(CountryService.currencyService);
        console.log(di.currencyService);
    }
    async findAll(query) {
        return await CountryService.countryRepository.find(pagination_1.Pagination.paginateOption(query, {
            where: {
                deleteFlag: false,
            },
        }));
    }
    async findByCode(code) {
        return await CountryService.countryRepository
            .findOne({
            where: { code },
        });
    }
    async findByCodeWithDeleteFlag(code) {
        return await CountryService.countryRepository
            .findOne({
            where: {
                code,
                deleteFlag: false,
            },
            relations: ["currency"]
        });
    }
    async create(data) {
        const exists = await this.findByCode(data.code);
        if (exists) {
            throw new error_1.DuplicateEntryError("Data already exists!");
        }
        const currency = await CountryService.currencyService.findByCodeWithDeleteFlag(data.currencyCode);
        if (!currency) {
            throw new error_1.DataNotFoundError(`Currency ${data.currencyCode} does not exist`);
        }
        const country = new entity_1.Country();
        country.code = data.code;
        country.name = data.name;
        country.currencyCode = data.currencyCode;
        country.dateCreated = new Date();
        country.lastUpdated = new Date();
        country.createdBy = "Admin";
        country.updatedBy = "Admin";
        country.currency = currency;
        try {
            return await CountryService.countryRepository.save(country);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newCountry) {
        const country = await this.findByCodeWithDeleteFlag(newCountry.code);
        if (country == null) {
            throw new Error(`Country not found ${newCountry.code}`);
        }
        if (newCountry.name !== null || newCountry.name !== undefined) {
            country.name = newCountry.name;
        }
        if (newCountry.currencyCode !== null || newCountry.currencyCode !== undefined) {
            const currency = await CountryService.currencyService.findByCodeWithDeleteFlag(newCountry.currencyCode);
            if (!currency) {
                throw new error_1.DataNotFoundError(`Currency ${newCountry.currencyCode} does not exist`);
            }
            country.currency = newCountry.currency;
        }
        country.lastUpdated = new Date();
        country.updatedBy = "Admin";
        return await CountryService.countryRepository.save(country);
    }
    async delete(code) {
        const country = await this.findByCodeWithDeleteFlag(code);
        if (country == null) {
            throw new error_1.DataNotFoundError(`Country not found: ${code}`);
        }
        country.deleteFlag = true;
        await CountryService.countryRepository.save(country);
        return country;
    }
}
exports.CountryService = CountryService;
CountryService.countryRepository = null;
CountryService.currencyService = null;
//# sourceMappingURL=countryService.js.map