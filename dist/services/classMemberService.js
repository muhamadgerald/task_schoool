"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClassMemberService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
class ClassMemberService {
    constructor(di) {
        if (ClassMemberService.classMemberRepository === null) {
            ClassMemberService.classMemberRepository = di.appTypeOrm.getConnection().getRepository(entity_1.ClassMember);
        }
    }
    async findAll(query) {
        return await ClassMemberService.classMemberRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    // public async findById(id: number): Promise<ClassMember> {
    //     try {
    //         return await ClassMemberService.classMemberRepository
    //             .findOne({
    //                 where: {
    //                     id
    //                 }
    //             });
    //     } catch (e) {
    //         console.log(e);
    //         throw new DataNotFoundError("Invalid code");
    //     }
    // }
    async create(data) {
        const classMember = new entity_1.ClassMember();
        classMember.classID = data.classID;
        classMember.student_id = data.student_id;
        console.log('DARI CLASS MEMBER SERVICE', classMember);
        try {
            return await ClassMemberService.classMemberRepository.save(classMember);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
}
exports.ClassMemberService = ClassMemberService;
ClassMemberService.classMemberRepository = null;
//# sourceMappingURL=classMemberService.js.map