"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NegaraService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class NegaraService {
    constructor(di) {
        if (NegaraService.negaraRepository === null) {
            NegaraService.negaraRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Negara);
        }
    }
    async findAll(query) {
        return await NegaraService.negaraRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await NegaraService.negaraRepository
                .findOne({
                where: {
                    id
                },
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const negara = new entity_1.Negara();
        negara.name = data.name;
        try {
            return await NegaraService.negaraRepository.save(negara);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newNegara) {
        const negara = await this.findById(newNegara.id);
        if (negara == null) {
            throw new error_1.DataNotFoundError(`Post not found: ${newNegara.id}`);
        }
        if (newNegara.name !== null || newNegara.name !== undefined) {
            negara.name = newNegara.name;
        }
        return await NegaraService.negaraRepository.save(negara);
    }
    async delete(code) {
        // const post: Post = await this.findById(code);
        // if (post == null) {
        //     throw new DataNotFoundError(`Post not found: ${code}`);
        // }
        // await PostService.postRepository.delete(post);
        // return post;
        const negara = await this.findById(code);
        await NegaraService.negaraRepository.delete(code);
        return negara;
    }
}
exports.NegaraService = NegaraService;
NegaraService.negaraRepository = null;
//# sourceMappingURL=negaraService.js.map