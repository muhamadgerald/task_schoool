"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClassService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const teacherService_1 = require("./teacherService");
const studentService_1 = require("./studentService");
const classMemberService_1 = require("./classMemberService");
// type createClass = {
//     name: string,
//     homeroomTeacherId: number,
//     year: number,
//     classId: number,
//     studentId: number
// }
class ClassService {
    constructor(di) {
        if (ClassService.classRepository === null) {
            ClassService.classRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Class);
        }
        if (ClassService.teacherRepository === null) {
            ClassService.teacherRepository = new teacherService_1.TeacherService(di);
        }
        if (ClassService.studentRepository === null) {
            ClassService.studentRepository = new studentService_1.StudentService(di);
        }
        if (ClassService.classMemberRepository === null) {
            ClassService.classMemberRepository = new classMemberService_1.ClassMemberService(di);
        }
    }
    async findAll(query) {
        return await ClassService.classRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await ClassService.classRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const checkTeacher = await ClassService.teacherRepository.findById(data.homeroomTeacherId);
        if (!checkTeacher) {
            throw new error_1.DataNotFoundError(`Teacher ${data.homeroomTeacherId} does not exist`);
        }
        for (let i = 0; i < data.students.length; i++) {
            const checkStudents = await ClassService.studentRepository.findById(data.students[i].studentId);
            if (!checkStudents) {
                throw new error_1.DataNotFoundError(`Student ${data.students[i].studentId} does not exist`);
            }
        }
        const classEntity = new entity_1.Class();
        classEntity.name = data.name;
        classEntity.homeroomTeacherId = data.homeroomTeacherId;
        classEntity.year = data.year;
        const classEntitySave = await ClassService.classRepository.save(classEntity);
        const classEntityId = classEntitySave.id;
        for (let i = 0; i < data.students.length; i++) {
            const classMember = new entity_1.ClassMember();
            classMember.classID = classEntityId;
            classMember.student_id = data.students[i].studentId;
            console.log('dari class service', classMember);
            await ClassService.classMemberRepository.create(classMember);
        }
        try {
            return classEntitySave;
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const classEntity = await this.findById(data.id);
        if (classEntity == null) {
            throw new error_1.DataNotFoundError(`Class not found: ${data.id}`);
        }
        if (data.name !== null || data.name !== undefined) {
            classEntity.name = data.name;
        }
        if (data.year !== null || data.year !== undefined) {
            classEntity.year = data.year;
        }
        if (data.homeroomTeacherId !== null || data.homeroomTeacherId !== undefined) {
            classEntity.homeroomTeacherId = data.homeroomTeacherId;
        }
        return await ClassService.classRepository.save(classEntity);
    }
    async delete(code) {
        const classEntity = await this.findById(code);
        if (classEntity == null) {
            throw new error_1.DataNotFoundError(`Class not found: ${code}`);
        }
        await ClassService.classRepository.delete(classEntity);
        return classEntity;
    }
}
exports.ClassService = ClassService;
ClassService.classRepository = null;
ClassService.teacherRepository = null;
ClassService.studentRepository = null;
ClassService.classMemberRepository = null;
//# sourceMappingURL=classService.js.map