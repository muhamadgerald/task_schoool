"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubjectScoreService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class SubjectScoreService {
    constructor(di) {
        if (SubjectScoreService.subjectScoreRepository === null) {
            SubjectScoreService.subjectScoreRepository = di.appTypeOrm.getConnection().getRepository(entity_1.SubjectScore);
        }
    }
    async findAll(query) {
        return await SubjectScoreService.subjectScoreRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['classId', 'subjectId', 'studentId']
        }));
    }
    async findById(id) {
        try {
            return await SubjectScoreService.subjectScoreRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const subjectScore = new entity_1.SubjectScore();
        subjectScore.classId = data.classId;
        subjectScore.subjectId = data.subjectId;
        subjectScore.studentId = data.studentId;
        subjectScore.term_1 = data.term_1;
        subjectScore.term_2 = data.term_2;
        subjectScore.term_3 = data.term_3;
        try {
            return await SubjectScoreService.subjectScoreRepository.save(subjectScore);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const subjectScore = await this.findById(data.id);
        if (subjectScore == null) {
            throw new error_1.DataNotFoundError(`Subject Score not found: ${data.id}`);
        }
        if (data.classId !== null || data.classId !== undefined) {
            subjectScore.classId = data.classId;
        }
        if (data.subjectId !== null || data.subjectId !== undefined) {
            subjectScore.subjectId = data.subjectId;
        }
        if (data.studentId !== null || data.studentId !== undefined) {
            subjectScore.studentId = data.studentId;
        }
        if (data.term_1 !== null || data.term_1 !== undefined) {
            subjectScore.term_1 = data.term_1;
        }
        if (data.term_2 !== null || data.term_2 !== undefined) {
            subjectScore.term_2 = data.term_2;
        }
        if (data.term_3 !== null || data.term_3 !== undefined) {
            subjectScore.term_3 = data.term_3;
        }
        return await SubjectScoreService.subjectScoreRepository.save(subjectScore);
    }
    async delete(code) {
        const subjectScore = await this.findById(code);
        if (subjectScore == null) {
            throw new error_1.DataNotFoundError(`Subject Score not found: ${code}`);
        }
        await SubjectScoreService.subjectScoreRepository.delete(subjectScore);
        return subjectScore;
    }
}
exports.SubjectScoreService = SubjectScoreService;
SubjectScoreService.subjectScoreRepository = null;
//# sourceMappingURL=subjectScoreService.js.map