"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class ProductService {
    constructor(di) {
        if (ProductService.productRepository === null) {
            ProductService.productRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Product);
        }
    }
    async findAll(query) {
        return await ProductService.productRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await ProductService.productRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const product = new entity_1.Product();
        product.name = data.name;
        product.stock = data.stock;
        try {
            return await ProductService.productRepository.save(product);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newProduct) {
        const product = await this.findById(newProduct.id);
        if (product == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${newProduct.id}`);
        }
        if (newProduct.name !== null || newProduct.name !== undefined) {
            product.name = newProduct.name;
        }
        if (newProduct.stock !== null || newProduct.stock !== undefined) {
            product.stock = newProduct.stock;
        }
        return await ProductService.productRepository.save(product);
    }
    async adjustStock(productId, adjustmentStock) {
        const product = await this.findById(productId);
        const newStock = product.stock + adjustmentStock;
        product.stock = newStock;
        return await ProductService.productRepository.save(product);
    }
    async delete(code) {
        const product = await this.findById(code);
        if (product == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await ProductService.productRepository.delete(product);
        return product;
    }
}
exports.ProductService = ProductService;
ProductService.productRepository = null;
//# sourceMappingURL=productService.js.map