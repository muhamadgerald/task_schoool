"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseInvoiceDetailService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const productService_1 = require("./productService");
class PurchaseInvoiceDetailService {
    constructor(di) {
        if (PurchaseInvoiceDetailService.purchaseInvoiceDetailRepository === null) {
            PurchaseInvoiceDetailService.purchaseInvoiceDetailRepository = di.appTypeOrm.getConnection().getRepository(entity_1.PurchaseInvoiceDetail);
        }
        if (PurchaseInvoiceDetailService.productService === null) {
            PurchaseInvoiceDetailService.productService = new productService_1.ProductService(di);
        }
        console.log(PurchaseInvoiceDetailService.productService);
        console.log(di.productService);
    }
    async findAll(query) {
        return await PurchaseInvoiceDetailService.purchaseInvoiceDetailRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['product', 'purchaseInvoice']
        }));
    }
    async findById(id) {
        try {
            return await PurchaseInvoiceDetailService.purchaseInvoiceDetailRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const purchaseInvoiceDetail = new entity_1.PurchaseInvoiceDetail();
        purchaseInvoiceDetail.product = data.product;
        purchaseInvoiceDetail.price = data.price;
        purchaseInvoiceDetail.purchaseInvoice = data.purchaseInvoice;
        try {
            return await PurchaseInvoiceDetailService.purchaseInvoiceDetailRepository.save(purchaseInvoiceDetail);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const purchaseInvoiceDetail = await this.findById(data.id);
        if (purchaseInvoiceDetail == null) {
            throw new error_1.DataNotFoundError(`Purchase Invoice not found: ${data.id}`);
        }
        if (data.product !== null || data.product !== undefined) {
            purchaseInvoiceDetail.product = data.product;
        }
        if (data.price !== null || data.price !== undefined) {
            purchaseInvoiceDetail.price = data.price;
        }
        if (data.purchaseInvoice !== null || data.purchaseInvoice !== undefined) {
            purchaseInvoiceDetail.purchaseInvoice = data.purchaseInvoice;
        }
        return await PurchaseInvoiceDetailService.purchaseInvoiceDetailRepository.save(purchaseInvoiceDetail);
    }
    async delete(code) {
        const purchaseInvoiceDetail = await this.findById(code);
        if (purchaseInvoiceDetail == null) {
            throw new error_1.DataNotFoundError(`Purchase Invoice not found: ${code}`);
        }
        await PurchaseInvoiceDetailService.purchaseInvoiceDetailRepository.delete(purchaseInvoiceDetail);
        return purchaseInvoiceDetail;
    }
}
exports.PurchaseInvoiceDetailService = PurchaseInvoiceDetailService;
PurchaseInvoiceDetailService.purchaseInvoiceDetailRepository = null;
PurchaseInvoiceDetailService.productService = null;
//# sourceMappingURL=purchaseInvoiceDetailService.js.map