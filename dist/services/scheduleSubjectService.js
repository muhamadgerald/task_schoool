"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScheduleSubjectService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
class ScheduleSubjectService {
    constructor(di) {
        if (ScheduleSubjectService.scheduleSubjectRepository === null) {
            ScheduleSubjectService.scheduleSubjectRepository = di.appTypeOrm.getConnection().getRepository(entity_1.ScheduleSubject);
        }
    }
    async findAll(query) {
        return await ScheduleSubjectService.scheduleSubjectRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async create(data) {
        const scheduleSubject = new entity_1.ScheduleSubject();
        scheduleSubject.scheduleDayId = data.scheduleDayId;
        scheduleSubject.subjectId = data.subjectId;
        scheduleSubject.teacherId = data.teacherId;
        scheduleSubject.startTime = data.startTime;
        scheduleSubject.endTime = data.endTime;
        try {
            return await ScheduleSubjectService.scheduleSubjectRepository.save(scheduleSubject);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
}
exports.ScheduleSubjectService = ScheduleSubjectService;
ScheduleSubjectService.scheduleSubjectRepository = null;
//# sourceMappingURL=scheduleSubjectService.js.map