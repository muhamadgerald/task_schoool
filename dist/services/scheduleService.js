"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScheduleService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const classService_1 = require("./classService");
const teacherService_1 = require("./teacherService");
const subjectService_1 = require("./subjectService");
const scheduleSubjectService_1 = require("./scheduleSubjectService");
class ScheduleService {
    constructor(di) {
        if (ScheduleService.scheduleRepository === null) {
            ScheduleService.scheduleRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Schedule);
        }
        if (ScheduleService.classRepository === null) {
            ScheduleService.classRepository = new classService_1.ClassService(di);
        }
        if (ScheduleService.teacherRepository === null) {
            ScheduleService.teacherRepository = new teacherService_1.TeacherService(di);
        }
        if (ScheduleService.subjectRepository === null) {
            ScheduleService.subjectRepository = new subjectService_1.SubjectService(di);
        }
        if (ScheduleService.scheduleSubject === null) {
            ScheduleService.scheduleSubject = new scheduleSubjectService_1.ScheduleSubjectService(di);
        }
    }
    async findAll(query) {
        return await ScheduleService.scheduleRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['class']
        }));
    }
    async findById(id) {
        try {
            return await ScheduleService.scheduleRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const checkClass = await ScheduleService.classRepository.findById(data.classId);
        if (!checkClass) {
            throw new error_1.DataNotFoundError(`Class ${data.classId} does not exist`);
        }
        const checkSubject = await ScheduleService.subjectRepository.findById(data.subjectId);
        if (!checkSubject) {
            throw new error_1.DataNotFoundError(`Subject ${data.classId} does not exist`);
        }
        const checkTeacher = await ScheduleService.teacherRepository.findById(data.teacherId);
        if (!checkTeacher) {
            throw new error_1.DataNotFoundError(`Teacher ${data.classId} does not exist`);
        }
        const schedule = new entity_1.Schedule();
        schedule.classId = data.classId;
        schedule.day = data.day;
        const scheduleSave = await ScheduleService.scheduleRepository.save(schedule);
        const scheduleId = scheduleSave.id;
        const scheduleSubject = new entity_1.ScheduleSubject();
        scheduleSubject.scheduleDayId = scheduleId;
        scheduleSubject.subjectId = data.subjectId;
        scheduleSubject.teacherId = data.teacherId;
        scheduleSubject.startTime = data.startTime;
        scheduleSubject.endTime = data.endTime;
        await ScheduleService.scheduleSubject.create(scheduleSubject);
        try {
            return schedule;
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const schedule = await this.findById(data.id);
        if (schedule == null) {
            throw new error_1.DataNotFoundError(`Schedule not found: ${data.id}`);
        }
        if (data.classId !== null || data.classId !== undefined) {
            schedule.classId = data.classId;
        }
        if (data.day !== null || data.day !== undefined) {
            schedule.day = data.day;
        }
        return await ScheduleService.scheduleRepository.save(schedule);
    }
    async delete(code) {
        const schedule = await this.findById(code);
        if (schedule == null) {
            throw new error_1.DataNotFoundError(`Schedule not found: ${code}`);
        }
        await ScheduleService.scheduleRepository.delete(schedule);
        return schedule;
    }
}
exports.ScheduleService = ScheduleService;
ScheduleService.scheduleRepository = null;
ScheduleService.classRepository = null;
ScheduleService.teacherRepository = null;
ScheduleService.subjectRepository = null;
ScheduleService.scheduleSubject = null;
//# sourceMappingURL=scheduleService.js.map