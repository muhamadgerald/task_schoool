"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StockOpnameService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const productService_1 = require("./productService");
class StockOpnameService {
    constructor(di) {
        if (StockOpnameService.stockOpnameRepository === null) {
            StockOpnameService.stockOpnameRepository = di.appTypeOrm.getConnection().getRepository(entity_1.StockOpname);
        }
        if (StockOpnameService.productService === null) {
            StockOpnameService.productService = new productService_1.ProductService(di);
        }
        console.log(StockOpnameService.productService);
        console.log(di.productService);
    }
    async findAll(query) {
        return await StockOpnameService.stockOpnameRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['product']
        }));
    }
    async findById(id) {
        try {
            return await StockOpnameService.stockOpnameRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const stockOpname = new entity_1.StockOpname();
        stockOpname.product = data.product;
        stockOpname.date = data.date;
        stockOpname.note = data.note;
        stockOpname.stock = data.stock;
        const saveStockOpname = await StockOpnameService.stockOpnameRepository.save(stockOpname);
        console.log(saveStockOpname.stock);
        await StockOpnameService.productService.adjustStock(saveStockOpname.product, saveStockOpname.stock);
        try {
            return saveStockOpname;
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const stockOpname = await this.findById(data.id);
        if (stockOpname == null) {
            throw new error_1.DataNotFoundError(`Stock Opname not found: ${data.id}`);
        }
        if (data.product !== null || data.product !== undefined) {
            stockOpname.product = data.product;
        }
        if (data.note !== null || data.note !== undefined) {
            stockOpname.note = data.note;
        }
        if (data.stock !== null || data.stock !== undefined) {
            stockOpname.stock = data.stock;
        }
        return await StockOpnameService.stockOpnameRepository.save(stockOpname);
    }
    async delete(code) {
        const stockOpname = await this.findById(code);
        if (stockOpname == null) {
            throw new error_1.DataNotFoundError(`Stock Opname not found: ${code}`);
        }
        await StockOpnameService.stockOpnameRepository.delete(stockOpname);
        console.log(stockOpname.stock);
        await StockOpnameService.productService.adjustStock(stockOpname.product, -stockOpname.stock);
        return stockOpname;
    }
}
exports.StockOpnameService = StockOpnameService;
StockOpnameService.stockOpnameRepository = null;
StockOpnameService.productService = null;
//# sourceMappingURL=stockOpnameService.js.map