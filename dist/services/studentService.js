"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StudentService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class StudentService {
    constructor(di) {
        if (StudentService.studentRepository === null) {
            StudentService.studentRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Student);
        }
    }
    async findAll(query) {
        return await StudentService.studentRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await StudentService.studentRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const student = new entity_1.Student();
        student.name = data.name;
        try {
            return await StudentService.studentRepository.save(student);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const student = await this.findById(data.id);
        if (student == null) {
            throw new error_1.DataNotFoundError(`Student not found: ${data.id}`);
        }
        if (data.name !== null || data.name !== undefined) {
            student.name = data.name;
        }
        return await StudentService.studentRepository.save(student);
    }
    async delete(code) {
        const student = await this.findById(code);
        if (student == null) {
            throw new error_1.DataNotFoundError(`Student not found: ${code}`);
        }
        await StudentService.studentRepository.delete(student);
        return student;
    }
}
exports.StudentService = StudentService;
StudentService.studentRepository = null;
//# sourceMappingURL=studentService.js.map