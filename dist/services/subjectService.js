"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubjectService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class SubjectService {
    constructor(di) {
        if (SubjectService.subjectRepository === null) {
            SubjectService.subjectRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Subject);
        }
    }
    async findAll(query) {
        return await SubjectService.subjectRepository.find(pagination_1.Pagination.paginateOption(query));
    }
    async findById(id) {
        try {
            return await SubjectService.subjectRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const subject = new entity_1.Subject();
        subject.name = data.name;
        try {
            return await SubjectService.subjectRepository.save(subject);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const subject = await this.findById(data.id);
        if (subject == null) {
            throw new error_1.DataNotFoundError(`Subject not found: ${data.id}`);
        }
        if (data.name !== null || data.name !== undefined) {
            subject.name = data.name;
        }
        return await SubjectService.subjectRepository.save(subject);
    }
    async delete(code) {
        const subject = await this.findById(code);
        if (subject == null) {
            throw new error_1.DataNotFoundError(`Subject not found: ${code}`);
        }
        await SubjectService.subjectRepository.delete(subject);
        return subject;
    }
}
exports.SubjectService = SubjectService;
SubjectService.subjectRepository = null;
//# sourceMappingURL=subjectService.js.map