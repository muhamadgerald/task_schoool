"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrencyService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class CurrencyService {
    constructor(di) {
        if (CurrencyService.currencyRepository === null) {
            CurrencyService.currencyRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Currency);
        }
    }
    async findAll(query) {
        return await CurrencyService.currencyRepository.find(pagination_1.Pagination.paginateOption(query, {
            where: {
                deleteFlag: false,
            },
        }));
    }
    async findByCodeWithDeleteFlag(code) {
        try {
            return await CurrencyService.currencyRepository.findOne({
                where: {
                    code,
                    deleteFlag: false,
                },
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async findByCode(code) {
        try {
            return await CurrencyService.currencyRepository
                .findOne({
                where: {
                    code,
                },
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const existing = await this.findByCode(data.code);
        if (existing) {
            throw new error_1.DuplicateEntryError("Data already exists!");
        }
        const currency = new entity_1.Currency();
        currency.createdBy = "Admin";
        currency.updatedBy = "Admin";
        currency.dateCreated = new Date();
        currency.lastUpdated = new Date();
        currency.code = data.code;
        currency.name = data.name;
        try {
            return await CurrencyService.currencyRepository.save(currency);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newCurrency) {
        const currency = await this.findByCodeWithDeleteFlag(newCurrency.code);
        if (currency == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${newCurrency.code}`);
        }
        if (newCurrency.name !== null || newCurrency.name !== undefined) {
            currency.name = newCurrency.name;
        }
        if (newCurrency.code !== null || newCurrency.code !== undefined) {
            currency.code = newCurrency.code;
        }
        currency.lastUpdated = new Date();
        currency.updatedBy = "Admin";
        return await CurrencyService.currencyRepository.save(currency);
    }
    async delete(code) {
        const currency = await this.findByCode(code);
        if (currency == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        currency.deleteFlag = true;
        await CurrencyService.currencyRepository.save(currency);
        return currency;
    }
}
exports.CurrencyService = CurrencyService;
CurrencyService.currencyRepository = null;
//# sourceMappingURL=currencyService.js.map