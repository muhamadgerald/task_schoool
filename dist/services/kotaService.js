"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KotaService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
class KotaService {
    constructor(di) {
        if (KotaService.kotaRepository === null) {
            KotaService.kotaRepository = di.appTypeOrm.getConnection().getRepository(entity_1.Kota);
        }
    }
    async findAll(query) {
        return await KotaService.kotaRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['provinsi']
        }));
    }
    async findById(id) {
        try {
            return await KotaService.kotaRepository
                .findOne({
                where: {
                    id
                },
                relations: ['provinsi']
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const kota = new entity_1.Kota();
        kota.name = data.name;
        kota.provinsi = data.provinsi;
        try {
            return await KotaService.kotaRepository.save(kota);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(newKota) {
        const kota = await this.findById(newKota.id);
        if (kota == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${newKota.id}`);
        }
        if (newKota.name !== null || newKota.name !== undefined) {
            kota.name = newKota.name;
        }
        if (newKota.provinsi !== null || newKota.provinsi !== undefined) {
            kota.provinsi = newKota.provinsi;
        }
        return await KotaService.kotaRepository.save(kota);
    }
    async delete(code) {
        const kota = await this.findById(code);
        if (kota == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await KotaService.kotaRepository.delete(kota);
        return kota;
    }
}
exports.KotaService = KotaService;
KotaService.kotaRepository = null;
//# sourceMappingURL=kotaService.js.map