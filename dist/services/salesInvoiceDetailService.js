"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SalesInvoiceDetailService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const productService_1 = require("./productService");
class SalesInvoiceDetailService {
    constructor(di) {
        if (SalesInvoiceDetailService.salesInvoiceDetailRepository === null) {
            SalesInvoiceDetailService.salesInvoiceDetailRepository = di.appTypeOrm.getConnection().getRepository(entity_1.SalesInvoiceDetail);
        }
        if (SalesInvoiceDetailService.productService === null) {
            SalesInvoiceDetailService.productService = new productService_1.ProductService(di);
        }
        console.log(SalesInvoiceDetailService.productService);
        console.log(di.productService);
    }
    async findAll(query) {
        return await SalesInvoiceDetailService.salesInvoiceDetailRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['product', 'salesInvoice']
        }));
    }
    async findById(id) {
        try {
            return await SalesInvoiceDetailService.salesInvoiceDetailRepository
                .findOne({
                where: {
                    id
                }
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        const salesInvoiceDetail = new entity_1.SalesInvoiceDetail();
        salesInvoiceDetail.product = data.product;
        salesInvoiceDetail.price = data.price;
        salesInvoiceDetail.salesInvoice = data.salesInvoice;
        try {
            return await SalesInvoiceDetailService.salesInvoiceDetailRepository.save(salesInvoiceDetail);
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const salesInvoiceDetail = await this.findById(data.id);
        if (salesInvoiceDetail == null) {
            throw new error_1.DataNotFoundError(`Sales Invoice not found: ${data.id}`);
        }
        if (data.product !== null || data.product !== undefined) {
            salesInvoiceDetail.product = data.product;
        }
        if (data.price !== null || data.price !== undefined) {
            salesInvoiceDetail.price = data.price;
        }
        if (data.salesInvoice !== null || data.salesInvoice !== undefined) {
            salesInvoiceDetail.salesInvoice = data.salesInvoice;
        }
        return await SalesInvoiceDetailService.salesInvoiceDetailRepository.save(salesInvoiceDetail);
    }
    async delete(code) {
        const salesInvoiceDetail = await this.findById(code);
        if (salesInvoiceDetail == null) {
            throw new error_1.DataNotFoundError(`Sales Invoice not found: ${code}`);
        }
        await SalesInvoiceDetailService.salesInvoiceDetailRepository.delete(salesInvoiceDetail);
        return salesInvoiceDetail;
    }
}
exports.SalesInvoiceDetailService = SalesInvoiceDetailService;
SalesInvoiceDetailService.salesInvoiceDetailRepository = null;
SalesInvoiceDetailService.productService = null;
//# sourceMappingURL=salesInvoiceDetailService.js.map