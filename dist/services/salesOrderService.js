"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SalesOrderService = void 0;
const entity_1 = require("../entity");
const pagination_1 = require("../lib/helper/pagination");
const error_1 = require("../error");
const salesOrderDetailService_1 = require("./salesOrderDetailService");
const customerService_1 = require("./customerService");
const productService_1 = require("./productService");
class SalesOrderService {
    constructor(di) {
        if (SalesOrderService.salesOrderRepository === null) {
            SalesOrderService.salesOrderRepository = di.appTypeOrm.getConnection().getRepository(entity_1.SalesOrder);
        }
        if (SalesOrderService.salesOrderDetailService === null) {
            SalesOrderService.salesOrderDetailService = new salesOrderDetailService_1.SalesOrderDetailService(di);
        }
        if (SalesOrderService.customerService === null) {
            SalesOrderService.customerService = new customerService_1.CustomerService(di);
        }
        if (SalesOrderService.productService === null) {
            SalesOrderService.productService = new productService_1.ProductService(di);
        }
    }
    async findAll(query) {
        return await SalesOrderService.salesOrderRepository.find(pagination_1.Pagination.paginateOption(query, {
            relations: ['customer']
        }));
    }
    async findById(id) {
        try {
            return await SalesOrderService.salesOrderRepository
                .findOne({
                where: {
                    id
                },
                relations: ['customer']
            });
        }
        catch (e) {
            console.log(e);
            throw new error_1.DataNotFoundError("Invalid code");
        }
    }
    async create(data) {
        // const exists = await this.findById(data.id);
        // if (exists) {
        //     throw new DuplicateEntryError("Data already exists!");
        // }
        const checkCostumer = await SalesOrderService.customerService.findById(data.customer);
        if (!checkCostumer) {
            throw new error_1.DataNotFoundError(`Customer ${data.customer} does not exist`);
        }
        for (let i = 0; i < data.products.length; i++) {
            const checkProduct = await SalesOrderService.productService.findById(data.products[i].productId);
            if (!checkProduct) {
                throw new error_1.DataNotFoundError(`Product ${data.products[i].productId} does not exist`);
            }
        }
        const salesOrder = new entity_1.SalesOrder();
        salesOrder.customer = data.customer;
        salesOrder.date = data.date;
        const salesOrderSave = await SalesOrderService.salesOrderRepository.save(salesOrder);
        const salesOrderId = salesOrderSave.id;
        for (let i = 0; i < data.products.length; i++) {
            const salesOrderDetail = new entity_1.SalesOrderDetail();
            salesOrderDetail.product = data.products[i].productId;
            salesOrderDetail.salesOrder = salesOrderId;
            salesOrderDetail.quantity = data.products[i].quantity;
            await SalesOrderService.salesOrderDetailService.create(salesOrderDetail);
            // await SalesOrderService.productService.adjustStock(data.products[i].productId, - data.products[i].quantity);
        }
        try {
            return salesOrderSave;
        }
        catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
    async update(data) {
        const salesOrder = await this.findById(data.id);
        if (salesOrder == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${data.id}`);
        }
        if (data.customer !== null || data.customer !== undefined) {
            salesOrder.customer = data.customer;
        }
        if (data.date !== null || data.date !== undefined) {
            salesOrder.date = data.date;
        }
        return await SalesOrderService.salesOrderRepository.save(salesOrder);
    }
    async delete(code) {
        const salesOrder = await this.findById(code);
        if (salesOrder == null) {
            throw new error_1.DataNotFoundError(`Currency not found: ${code}`);
        }
        await SalesOrderService.salesOrderRepository.delete(salesOrder);
        return salesOrder;
    }
}
exports.SalesOrderService = SalesOrderService;
SalesOrderService.salesOrderRepository = null;
SalesOrderService.salesOrderDetailService = null;
SalesOrderService.productService = null;
SalesOrderService.customerService = null;
//# sourceMappingURL=salesOrderService.js.map