"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDiComponent = exports.getAppTypeOrm = void 0;
// tslint:disable:max-line-length
const le_validator_1 = require("le-validator");
const partial_responsify_1 = require("partial-responsify");
const entity_1 = require("../entity");
const lib_1 = require("../lib");
const services_1 = require("../services");
const classMemberService_1 = require("../services/classMemberService");
exports.getAppTypeOrm = async (config) => {
    return new lib_1.AppTypeOrm(await lib_1.AppTypeOrm.getConnectionManager({
        database: config.dbDatabase,
        entities: [
            entity_1.Subject,
            entity_1.Student,
            entity_1.Teacher,
            entity_1.Class,
            entity_1.ClassMember,
            entity_1.Schedule,
            entity_1.ScheduleSubject,
            entity_1.SubjectScore
        ],
        host: config.dbHost,
        logging: config.dbLogging,
        password: config.dbPassword,
        type: config.dbType,
        username: config.dbUsername,
    }));
};
exports.getDiComponent = async (config) => {
    const di = {
        appTypeOrm: await exports.getAppTypeOrm(config),
        config,
        helper: new lib_1.Helper(),
        partialResponsify: new partial_responsify_1.PartialResponsify(),
        validator: new le_validator_1.Validator(),
        subjectService: null,
        teacherService: null,
        studentService: null,
        classService: null,
        classMemberService: null,
        scheduleService: null,
        scheduleSubjectService: null,
        subjectScoreService: null
    };
    di.subjectService = new services_1.SubjectService(di);
    di.teacherService = new services_1.TeacherService(di);
    di.studentService = new services_1.StudentService(di);
    di.classService = new services_1.ClassService(di);
    di.classMemberService = new classMemberService_1.ClassMemberService(di);
    di.scheduleService = new services_1.ScheduleService(di);
    di.scheduleSubjectService = new services_1.ScheduleSubjectService(di);
    di.subjectScoreService = new services_1.SubjectScoreService(di);
    return di;
};
//# sourceMappingURL=index.js.map