"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppTypeOrm = void 0;
const typeorm_1 = require("typeorm");
const MigrationExecutor_1 = require("typeorm/migration/MigrationExecutor");
class AppTypeOrm {
    constructor(connectionManager) {
        this.connectionManager = connectionManager;
    }
    static async getConnectionManager(options) {
        const connectionManager = new typeorm_1.ConnectionManager();
        const connection = connectionManager.create(options);
        await connection.connect();
        return connectionManager;
    }
    getConnection() {
        return this.connectionManager.get();
    }
    async runMigration() {
        const migrationExecutor = new MigrationExecutor_1.MigrationExecutor(this.getConnection());
        // return migrationExecutor.undoLastMigration();
        return migrationExecutor.executePendingMigrations();
    }
}
exports.AppTypeOrm = AppTypeOrm;
//# sourceMappingURL=index.js.map