"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Helper = void 0;
const _ = require("lodash");
class Helper {
    restructureArrObject(arr, theMap) {
        const newArr = [];
        for (const obj of arr) {
            const newObj = {};
            for (const [mapkey, mapval] of theMap.entries()) {
                newObj[mapkey] = obj[mapval];
            }
            newArr.push(newObj);
        }
        return newArr;
    }
    /**
     * deprecated, use getFieldsToUseParents
     * @param fieldsToUse
     * @param relateMap Map<fieldToUseTup0, relation>
     */
    getIncludedRelations(fieldsToUse, relateMap) {
        const relationMap = new Map();
        for (const entry of relateMap.entries()) {
            // const fieldToUseTup0 = entry[0];
            const relation = entry[1];
            relationMap.set(relation, false);
        }
        for (const field of fieldsToUse) {
            const fieldToUseTup0 = field[0].join(",");
            if (relateMap.has(fieldToUseTup0)) {
                relationMap.set(fieldToUseTup0, true);
            }
        }
        const relations = [];
        for (const entry of relationMap.entries()) {
            const relation = entry[0];
            if (entry[1] === true) {
                relations.push(relation);
            }
        }
        return relations;
    }
    /**
     * @param fieldsToUse
     */
    getFieldsToUseRelations(fieldsToUse) {
        const relations = {};
        const fieldToUseTup0Set = new Set();
        for (const field of fieldsToUse) {
            const fieldToUseTup0 = field[0];
            fieldToUseTup0Set.add(fieldToUseTup0.join("."));
        }
        for (const fieldToUseTup0Str of fieldToUseTup0Set) {
            if (fieldToUseTup0Str === "") {
                continue;
            }
            const v = _.get(relations, fieldToUseTup0Str, null);
            if (v === null) {
                _.set(relations, fieldToUseTup0Str, true);
            }
        }
        return relations;
    }
    getFieldsToUseAsObject(fieldsToUse) {
        const obj = {};
        for (const field of fieldsToUse) {
            const fieldToUseTup0 = field[0];
            const fieldToUseTup1Str = field[1];
            const fieldToUseTup0Str = fieldToUseTup0.join(".");
            const str = fieldToUseTup0Str === "" ? fieldToUseTup1Str : fieldToUseTup0Str + "." + fieldToUseTup1Str;
            const v = _.get(obj, str, null);
            if (v === null) {
                _.set(obj, str, true);
            }
        }
        return obj;
    }
    numToString(num) {
        if (typeof num === "number") {
            return num.toString();
        }
        else {
            return num;
        }
    }
    dateToIsoString(date) {
        return date ? date.toISOString() : null;
    }
}
exports.Helper = Helper;
//# sourceMappingURL=helper.js.map