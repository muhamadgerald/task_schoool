"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Config = void 0;
class Config {
    constructor() {
        this.dbDatabase = process.env.DB_DATABASE || "task_school";
        this.dbHost = process.env.DB_HOST || "127.0.0.1";
        this.dbPassword = process.env.DB_PASSWORD || "";
        this.dbType = "mysql";
        this.dbUsername = process.env.DB_USERNAME || "root";
        this.dbSchema = process.env.DB_SCHEMA || "dbo";
        this.dbLogging = process.env.NODE_ENV === "development";
        this.dbMaxPool = Number.isNaN(parseInt(process.env.DB_MAX_POOL || "50", 10)) ? 50 : parseInt(process.env.DB_MAX_POOL || "50", 10);
        this.logger = process.env.LOGGER !== "FALSE";
        this.port = "3000"; // process.env.NODE_PORT || "3004"; // temporary change from 3000 to 80
        // public redisHost = process.env.REDIS_HOST; // temporary
        // public redisPassword = process.env.REDIS_PASSWORD; // temporary
    }
}
exports.Config = Config;
//# sourceMappingURL=index.js.map