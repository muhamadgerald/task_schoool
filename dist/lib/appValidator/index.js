"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PageableGetParam = exports.pageableGetParamValidator = void 0;
exports.pageableGetParamValidator = [{
        name: "page",
        required: false,
        swagger: {
            description: "Page",
            example: 1,
        },
        type: "number",
        default: 1,
    }, {
        name: "size",
        required: false,
        swagger: {
            description: "Size per page",
            example: 10,
        },
        type: "number",
        default: 10,
    }, {
        name: "sort",
        required: false,
        swagger: {
            description: "Sorting",
            example: "code,name,nameEn,nameTh",
        },
        type: "string",
        default: "",
    }];
class PageableGetParam {
}
exports.PageableGetParam = PageableGetParam;
//# sourceMappingURL=index.js.map