"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = require("node-fetch");
class ServiceBase {
    async sendRequest(path, method, data) {
        try {
            const url = `${this.baseUrl.replace(/\/+$/g, "")}/${path.replace(/^\//g, "")}`;
            const request = await node_fetch_1.default(url, Object.assign(Object.assign({}, data), { method }));
            return await request.json();
        }
        catch (error) {
            throw new Error(error);
        }
    }
}
exports.default = ServiceBase;
//# sourceMappingURL=serviceBase.js.map