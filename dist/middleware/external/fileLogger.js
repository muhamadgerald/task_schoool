"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const os = require("os");
const fs = require("fs");
const path = require("path");
const dayjs = require("dayjs");
const util = require("util");
const internal = {
    log: console.log,
    error: console.error,
    info: console.info,
    warn: console.warn,
};
const root = process.cwd();
const logsDir = path.resolve(root, "logs");
try {
    if (!fs.readdirSync(logsDir)) {
        fs.mkdirSync(logsDir);
    }
}
catch (_a) {
    fs.mkdirSync(logsDir);
}
async function writer(type, ...log) {
    const dayjsInstance = dayjs();
    const today = dayjsInstance.format("YYYYMMDD");
    const dateTime = dayjsInstance.format("YYYY-MM-DD HH:mm:ss");
    const logPath = path.resolve(logsDir, `idbe-riskprofile-${today}.log`);
    log.forEach((logToFile) => {
        fs.appendFileSync(logPath, `[${dateTime}] ` + util.inspect(logToFile, false, 100).replace(/\\n/g, ""));
        fs.appendFileSync(logPath, os.EOL);
    });
    internal[type](`[${dateTime}]`, ...log);
}
console.log = (...log) => {
    writer("log", ...log);
};
console.info = (...log) => {
    writer("info", ...log);
};
console.error = (...log) => {
    writer("error", ...log);
};
console.warn = (...log) => {
    writer("warn", ...log);
};
//# sourceMappingURL=fileLogger.js.map