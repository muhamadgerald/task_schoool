"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.di = void 0;
exports.di = (theDi) => async (ctx, next) => {
    ctx.state.di = theDi;
    await next();
};
//# sourceMappingURL=di.js.map