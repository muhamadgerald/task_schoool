"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
async function logger(ctx, next) {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    // tslint:disable-next-line:no-console
    console.log(`${ctx.method} ${ctx.url} ${ms}ms`);
}
exports.logger = logger;
//# sourceMappingURL=logger.js.map