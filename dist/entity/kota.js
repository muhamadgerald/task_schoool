"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Kota = void 0;
const typeorm_1 = require("typeorm");
const ekspidisi_1 = require("./ekspidisi");
const provinsi_1 = require("./provinsi");
let Kota = class Kota {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: "id" }),
    __metadata("design:type", String)
], Kota.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'name' }),
    __metadata("design:type", String)
], Kota.prototype, "name", void 0);
__decorate([
    typeorm_1.ManyToOne(() => provinsi_1.Provinsi, provinsi => provinsi.id),
    typeorm_1.JoinColumn({ name: "provinsi_id" }),
    __metadata("design:type", provinsi_1.Provinsi)
], Kota.prototype, "provinsi", void 0);
__decorate([
    typeorm_1.OneToMany(() => ekspidisi_1.Ekspidisi, (ekspidisi) => ekspidisi.destinationCity, { onDelete: 'CASCADE', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Kota.prototype, "destinationCity", void 0);
__decorate([
    typeorm_1.OneToMany(() => ekspidisi_1.Ekspidisi, (ekspidisi) => ekspidisi.originCity, { onDelete: 'CASCADE', onUpdate: 'CASCADE' }),
    __metadata("design:type", Array)
], Kota.prototype, "originCity", void 0);
Kota = __decorate([
    typeorm_1.Entity({
        name: "kota"
    })
], Kota);
exports.Kota = Kota;
//# sourceMappingURL=kota.js.map