"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScheduleSubject = void 0;
const typeorm_1 = require("typeorm");
const schedule_1 = require("./schedule");
const subject_1 = require("./subject");
const teacher_1 = require("./teacher");
let ScheduleSubject = class ScheduleSubject {
};
__decorate([
    typeorm_1.PrimaryColumn({ name: 'schedule_day_id' }),
    typeorm_1.ManyToOne(() => schedule_1.Schedule, schedule => schedule.id),
    typeorm_1.JoinColumn({ name: 'schedule_day_id' }),
    __metadata("design:type", schedule_1.Schedule)
], ScheduleSubject.prototype, "scheduleDayId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => subject_1.Subject, subject => subject.id),
    typeorm_1.JoinColumn({ name: 'subject_id' }),
    __metadata("design:type", subject_1.Subject)
], ScheduleSubject.prototype, "subjectId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => teacher_1.Teacher, teacher => teacher.id),
    typeorm_1.JoinColumn({ name: 'teacher_id' }),
    __metadata("design:type", teacher_1.Teacher)
], ScheduleSubject.prototype, "teacherId", void 0);
__decorate([
    typeorm_1.Column({ name: 'start_time' }),
    __metadata("design:type", Date)
], ScheduleSubject.prototype, "startTime", void 0);
__decorate([
    typeorm_1.Column({ name: 'end_time' }),
    __metadata("design:type", Date)
], ScheduleSubject.prototype, "endTime", void 0);
ScheduleSubject = __decorate([
    typeorm_1.Entity({ name: 'schedule_subject' })
], ScheduleSubject);
exports.ScheduleSubject = ScheduleSubject;
//# sourceMappingURL=scheduleSubject.js.map