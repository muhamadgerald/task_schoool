"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubjectScore = void 0;
const typeorm_1 = require("typeorm");
const class_1 = require("./class");
const student_1 = require("./student");
const subject_1 = require("./subject");
let SubjectScore = class SubjectScore {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: "id" }),
    __metadata("design:type", Number)
], SubjectScore.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(() => class_1.Class, classId => classId.id),
    typeorm_1.JoinColumn({ name: 'class_id' }),
    __metadata("design:type", Number)
], SubjectScore.prototype, "classId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => subject_1.Subject, subject => subject.id),
    typeorm_1.JoinColumn({ name: 'subject_id' }),
    __metadata("design:type", Number)
], SubjectScore.prototype, "subjectId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => student_1.Student, student => student.id),
    typeorm_1.JoinColumn({ name: 'student_id' }),
    __metadata("design:type", Number)
], SubjectScore.prototype, "studentId", void 0);
__decorate([
    typeorm_1.Column({ name: 'term_1' }),
    __metadata("design:type", String)
], SubjectScore.prototype, "term_1", void 0);
__decorate([
    typeorm_1.Column({ name: 'term_2' }),
    __metadata("design:type", String)
], SubjectScore.prototype, "term_2", void 0);
__decorate([
    typeorm_1.Column({ name: 'term_3' }),
    __metadata("design:type", String)
], SubjectScore.prototype, "term_3", void 0);
SubjectScore = __decorate([
    typeorm_1.Entity({ name: 'subject_score' })
], SubjectScore);
exports.SubjectScore = SubjectScore;
//# sourceMappingURL=subjectScore.js.map