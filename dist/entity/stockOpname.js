"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StockOpname = void 0;
const typeorm_1 = require("typeorm");
const product_1 = require("./product");
let StockOpname = class StockOpname {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: 'id' }),
    __metadata("design:type", Number)
], StockOpname.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(() => product_1.Product, product => product.id, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    }),
    typeorm_1.JoinColumn({ name: 'product_id' }),
    __metadata("design:type", product_1.Product)
], StockOpname.prototype, "product", void 0);
__decorate([
    typeorm_1.Column({ name: 'date' }),
    __metadata("design:type", Date)
], StockOpname.prototype, "date", void 0);
__decorate([
    typeorm_1.Column({ name: 'note' }),
    __metadata("design:type", String)
], StockOpname.prototype, "note", void 0);
__decorate([
    typeorm_1.Column({ name: 'stock' }),
    __metadata("design:type", Number)
], StockOpname.prototype, "stock", void 0);
StockOpname = __decorate([
    typeorm_1.Entity({
        name: 'stock_opname'
    })
], StockOpname);
exports.StockOpname = StockOpname;
//# sourceMappingURL=stockOpname.js.map