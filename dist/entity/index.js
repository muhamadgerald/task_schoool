"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(require("./subject"), exports);
__exportStar(require("./student"), exports);
__exportStar(require("./teacher"), exports);
__exportStar(require("./class"), exports);
__exportStar(require("./classMember"), exports);
__exportStar(require("./schedule"), exports);
__exportStar(require("./scheduleSubject"), exports);
__exportStar(require("./subjectScore"), exports);
//# sourceMappingURL=index.js.map