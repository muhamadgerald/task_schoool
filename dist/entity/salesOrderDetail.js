"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SalesOrderDetail = void 0;
const typeorm_1 = require("typeorm");
const product_1 = require("./product");
const salesOrder_1 = require("./salesOrder");
let SalesOrderDetail = class SalesOrderDetail {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: 'id' }),
    __metadata("design:type", Number)
], SalesOrderDetail.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(() => salesOrder_1.SalesOrder, salesOrder => salesOrder.id),
    typeorm_1.JoinColumn({ name: 'sales_order_id' }),
    __metadata("design:type", salesOrder_1.SalesOrder)
], SalesOrderDetail.prototype, "salesOrder", void 0);
__decorate([
    typeorm_1.ManyToOne(() => product_1.Product, product => product.id),
    typeorm_1.JoinColumn({ name: 'product_id' }),
    __metadata("design:type", product_1.Product)
], SalesOrderDetail.prototype, "product", void 0);
__decorate([
    typeorm_1.Column({ name: 'quantity' }),
    __metadata("design:type", Number)
], SalesOrderDetail.prototype, "quantity", void 0);
SalesOrderDetail = __decorate([
    typeorm_1.Entity({ name: 'sales_order_detail' })
], SalesOrderDetail);
exports.SalesOrderDetail = SalesOrderDetail;
//# sourceMappingURL=salesOrderDetail.js.map