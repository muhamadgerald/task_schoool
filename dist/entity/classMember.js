"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClassMember = void 0;
const typeorm_1 = require("typeorm");
const class_1 = require("./class");
const student_1 = require("./student");
let ClassMember = class ClassMember {
};
__decorate([
    typeorm_1.PrimaryColumn({ name: 'class_id' }),
    typeorm_1.ManyToOne(() => class_1.Class, classId => classId.id, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    }),
    typeorm_1.JoinColumn({ name: 'class_id' }),
    __metadata("design:type", class_1.Class)
], ClassMember.prototype, "classID", void 0);
__decorate([
    typeorm_1.PrimaryColumn({ name: 'student_id' }),
    typeorm_1.ManyToOne(() => student_1.Student, student => student.id),
    typeorm_1.JoinColumn({ name: 'student_id' }),
    __metadata("design:type", student_1.Student)
], ClassMember.prototype, "student_id", void 0);
ClassMember = __decorate([
    typeorm_1.Entity({ name: 'class_member' })
], ClassMember);
exports.ClassMember = ClassMember;
//# sourceMappingURL=classMember.js.map