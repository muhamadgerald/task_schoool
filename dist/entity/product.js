"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
const typeorm_1 = require("typeorm");
const purchaseInvoiceDetail_1 = require("./purchaseInvoiceDetail");
const purchaseOrderDetail_1 = require("./purchaseOrderDetail");
const salesInvoiceDetail_1 = require("./salesInvoiceDetail");
const salesOrderDetail_1 = require("./salesOrderDetail");
const stockOpname_1 = require("./stockOpname");
let Product = class Product {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: "id" }),
    __metadata("design:type", Number)
], Product.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'name' }),
    __metadata("design:type", String)
], Product.prototype, "name", void 0);
__decorate([
    typeorm_1.Column({ name: 'stock' }),
    __metadata("design:type", Number)
], Product.prototype, "stock", void 0);
__decorate([
    typeorm_1.OneToMany(() => salesOrderDetail_1.SalesOrderDetail, salesOrderDetail => salesOrderDetail.id),
    __metadata("design:type", Array)
], Product.prototype, "salesOrderDetail", void 0);
__decorate([
    typeorm_1.OneToMany(() => purchaseOrderDetail_1.PurchaseOrderDetail, purchaseOrderDetail => purchaseOrderDetail.id),
    __metadata("design:type", Array)
], Product.prototype, "purchaseOrderDetail", void 0);
__decorate([
    typeorm_1.OneToMany(() => purchaseInvoiceDetail_1.PurchaseInvoiceDetail, purchaseInvoiceDetail => purchaseInvoiceDetail.id),
    __metadata("design:type", Array)
], Product.prototype, "purchaseInvoiceDetail", void 0);
__decorate([
    typeorm_1.OneToMany(() => salesInvoiceDetail_1.SalesInvoiceDetail, salesInvoiceDetail => salesInvoiceDetail.id),
    __metadata("design:type", Array)
], Product.prototype, "salesInvoiceDetail", void 0);
__decorate([
    typeorm_1.OneToMany(() => stockOpname_1.StockOpname, stockOpname => stockOpname.id),
    __metadata("design:type", Array)
], Product.prototype, "stockOpname", void 0);
Product = __decorate([
    typeorm_1.Entity({
        name: "product"
    })
], Product);
exports.Product = Product;
//# sourceMappingURL=product.js.map