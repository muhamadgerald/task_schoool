"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseInvoiceDetail = void 0;
const typeorm_1 = require("typeorm");
const product_1 = require("./product");
const purchaseInvoice_1 = require("./purchaseInvoice");
let PurchaseInvoiceDetail = class PurchaseInvoiceDetail {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: 'id' }),
    __metadata("design:type", Number)
], PurchaseInvoiceDetail.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(() => purchaseInvoice_1.PurchaseInvoice, purchaseInvoice => purchaseInvoice.id),
    typeorm_1.JoinColumn({ name: 'purchase_invoice_id' }),
    __metadata("design:type", purchaseInvoice_1.PurchaseInvoice)
], PurchaseInvoiceDetail.prototype, "purchaseInvoice", void 0);
__decorate([
    typeorm_1.ManyToOne(() => product_1.Product, product => product.id),
    typeorm_1.JoinColumn({ name: 'product_id' }),
    __metadata("design:type", product_1.Product)
], PurchaseInvoiceDetail.prototype, "product", void 0);
__decorate([
    typeorm_1.Column({ name: 'price_per_item' }),
    __metadata("design:type", Number)
], PurchaseInvoiceDetail.prototype, "price", void 0);
PurchaseInvoiceDetail = __decorate([
    typeorm_1.Entity({
        name: 'purchase_invoice_detail'
    })
], PurchaseInvoiceDetail);
exports.PurchaseInvoiceDetail = PurchaseInvoiceDetail;
//# sourceMappingURL=purchaseInvoiceDetail.js.map