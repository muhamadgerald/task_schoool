"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Class = void 0;
const typeorm_1 = require("typeorm");
const teacher_1 = require("./teacher");
let Class = class Class {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: 'id' }),
    __metadata("design:type", Number)
], Class.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ name: 'name' }),
    __metadata("design:type", String)
], Class.prototype, "name", void 0);
__decorate([
    typeorm_1.ManyToOne(() => teacher_1.Teacher, teacher => teacher.id),
    typeorm_1.JoinColumn({ name: 'homeroom_teacher_id' }),
    __metadata("design:type", Number)
], Class.prototype, "homeroomTeacherId", void 0);
__decorate([
    typeorm_1.Column({ name: 'year' }),
    __metadata("design:type", Number)
], Class.prototype, "year", void 0);
Class = __decorate([
    typeorm_1.Entity({
        name: 'class'
    })
], Class);
exports.Class = Class;
//# sourceMappingURL=class.js.map