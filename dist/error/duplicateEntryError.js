"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DuplicateEntryError = void 0;
class DuplicateEntryError extends Error {
    constructor(field) {
        super(field + " has duplicate value");
        this.name = "DuplicateEntryError";
    }
}
exports.DuplicateEntryError = DuplicateEntryError;
//# sourceMappingURL=duplicateEntryError.js.map