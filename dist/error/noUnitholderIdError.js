"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NoUnitholderIdError = void 0;
class NoUnitholderIdError extends Error {
    constructor() {
        super("no unitholder ID");
        this.name = "NoUnitholderIdError";
    }
}
exports.NoUnitholderIdError = NoUnitholderIdError;
//# sourceMappingURL=noUnitholderIdError.js.map