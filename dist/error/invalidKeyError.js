"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidKeyError = void 0;
class InvalidKeyError extends Error {
    constructor(keys) {
        super(keys.join(", ") + " invalid");
        this.name = "InvalidKeyError";
    }
}
exports.InvalidKeyError = InvalidKeyError;
//# sourceMappingURL=invalidKeyError.js.map