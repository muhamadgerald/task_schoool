"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DocumentNotAllowUpdateError = void 0;
class DocumentNotAllowUpdateError extends Error {
    constructor() {
        super("Document status not allow update");
        this.name = "DocumentNotAllowUpdateError";
    }
}
exports.DocumentNotAllowUpdateError = DocumentNotAllowUpdateError;
//# sourceMappingURL=documentNotAllowUpdateError.js.map