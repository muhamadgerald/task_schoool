"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateRangeError = void 0;
class DateRangeError extends Error {
    constructor(field, formatMessage) {
        super(field + ": " + formatMessage);
        this.name = "DateRangeError";
    }
}
exports.DateRangeError = DateRangeError;
//# sourceMappingURL=dateRangeError.js.map