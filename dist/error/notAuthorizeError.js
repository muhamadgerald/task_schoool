"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotAuthorizeError = void 0;
class NotAuthorizeError extends Error {
    constructor() {
        super("Not Authorized");
        this.name = "NotAuthorizeError";
    }
}
exports.NotAuthorizeError = NotAuthorizeError;
//# sourceMappingURL=notAuthorizeError.js.map