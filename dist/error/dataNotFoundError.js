"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataNotFoundError = void 0;
class DataNotFoundError extends Error {
    constructor(message) {
        super(message ? `Data not found: ${message}` : "Data not found");
        this.name = "DataNotFoundError";
    }
}
exports.DataNotFoundError = DataNotFoundError;
//# sourceMappingURL=dataNotFoundError.js.map