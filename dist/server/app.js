"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getApp = void 0;
const Koa = require("koa");
// import * as cors from "koa2-cors";
require("reflect-metadata");
const di_1 = require("../di");
const lib_1 = require("../lib");
const middleware_1 = require("../middleware");
const appRoutes_1 = require("./appRoutes");
require("../middleware/external");
async function getApp() {
    const app = new Koa();
    const config = new lib_1.Config();
    app.use(async (ctx, next) => {
        console.log(`[${ctx.method}] ${ctx.request.url}`);
        await next();
    });
    app.use(middleware_1.errorCatcher);
    app.use(middleware_1.di(await di_1.getDiComponent(config)));
    app.use(middleware_1.commonHeader);
    // app.use(cors());
    app.use(async (ctx, next) => {
        const customStartTime = Date.now();
        console.log("RequestBody", ctx.request.body || ctx.params);
        console.log("Headers", ctx.request.headers);
        await next();
        console.log("ResponseCode", ctx.status);
        console.log("ResponseBody", ctx.body);
        console.log(`${Date.now() - customStartTime}ms`);
    });
    const appRoutes = new appRoutes_1.AppRoutes(false);
    app.use(appRoutes.getRoutes());
    // it is optional though, but test got this for testing purpose
    // app.use(appRoutes.getAllowedMethods());
    return { app, config };
}
exports.getApp = getApp;
//# sourceMappingURL=app.js.map