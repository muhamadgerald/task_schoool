"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppRoutes = void 0;
const bodyparser = require("koa-bodyparser");
const Router = require("koa-router");
const middleware_1 = require("../middleware");
const v1_1 = require("./allRoutes/api/v1");
const uptime_1 = require("./allRoutes/uptime");
class AppRoutes {
    constructor(needLogger) {
        const apiV1Router = new Router({
            prefix: "/api",
        });
        const apiRouter = new Router({
            prefix: "/api",
        });
        this.baseRouter = new Router();
        this.baseRouter.get("/", uptime_1.uptime);
        apiRouter.get("/", uptime_1.uptime);
        for (const baseRoute of v1_1.baseRoutes) {
            const args = [];
            if (needLogger) {
                args.push(middleware_1.logger);
            }
            if (baseRoute.method === "GET") {
                args.push(baseRoute.func);
                apiV1Router.get(baseRoute.path, ...args);
                if (baseRoute.tmppath) {
                    apiV1Router.get(baseRoute.tmppath, ...args);
                }
            }
            else if (baseRoute.method === "PUT") {
                args.push(bodyparser(), baseRoute.func);
                apiV1Router.put(baseRoute.path, ...args);
                if (baseRoute.tmppath) {
                    apiV1Router.put(baseRoute.tmppath, ...args);
                }
            }
            else if (baseRoute.method === "POST") {
                args.push(bodyparser(), baseRoute.func);
                apiV1Router.post(baseRoute.path, ...args);
                if (baseRoute.tmppath) {
                    apiV1Router.post(baseRoute.tmppath, ...args);
                }
            }
            else if (baseRoute.method === "PATCH") {
                args.push(bodyparser(), baseRoute.func);
                apiV1Router.patch(baseRoute.path, ...args);
                if (baseRoute.tmppath) {
                    apiV1Router.patch(baseRoute.tmppath, ...args);
                }
            }
            else if (baseRoute.method === "DELETE") {
                args.push(bodyparser(), baseRoute.func);
                apiV1Router.delete(baseRoute.path, ...args);
                if (baseRoute.tmppath) {
                    apiV1Router.delete(baseRoute.tmppath, ...args);
                }
            }
        }
        this.baseRouter.use("", apiRouter.routes());
        this.baseRouter.use("", apiV1Router.routes());
    }
    getRoutes() {
        return this.baseRouter.routes();
    }
    getAllowedMethods() {
        return this.baseRouter.allowedMethods();
    }
}
exports.AppRoutes = AppRoutes;
//# sourceMappingURL=appRoutes.js.map