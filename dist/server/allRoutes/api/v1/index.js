"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.baseRoutes = exports.baseOpenApiRoutes = void 0;
// ROUTE CONTROLER IMPORTS
// GET ROUTES
const getSubject_1 = require("./get/getSubject");
const getTeacher_1 = require("./get/getTeacher");
const getStudent_1 = require("./get/getStudent");
const getClass_1 = require("./get/getClass");
const getClassMember_1 = require("./get/getClassMember");
const getSchedule_1 = require("./get/getSchedule");
const getScheduleSubject_1 = require("./get/getScheduleSubject");
const getSubjectScore_1 = require("./get/getSubjectScore");
// POST ROUTES
const postSubject_1 = require("./post/postSubject");
const postTeacher_1 = require("./post/postTeacher");
const postStudent_1 = require("./post/postStudent");
const postClass_1 = require("./post/postClass");
const postSchedule_1 = require("./post/postSchedule");
const postSubjectScore_1 = require("./post/postSubjectScore");
// PUT ROUTES
const putSubject_1 = require("./put/putSubject");
const putTeacher_1 = require("./put/putTeacher");
const putStudent_1 = require("./put/putStudent");
const putClass_1 = require("./put/putClass");
// DELETE ROUTES
const deleteSubject_1 = require("./delete/deleteSubject");
const deleteTeacher_1 = require("./delete/deleteTeacher");
const deleteStudent_1 = require("./delete/deleteStudent");
const deleteClass_1 = require("./delete/deleteClass");
// REGISTER THE ROUTE CONTROLLER HERE
const routes = [
    // [openapiGetProduct, getProduct],
    [getSubject_1.openapiGetSubject, getSubject_1.getSubject],
    [getTeacher_1.openapiGetTeacher, getTeacher_1.getTeacher],
    [getStudent_1.openapiGetStudent, getStudent_1.getStudent],
    [getClass_1.openapiGetClasses, getClass_1.getClasses],
    [getClassMember_1.openapiGetClassMember, getClassMember_1.getClassMember],
    [getSchedule_1.openapiGetSchedule, getSchedule_1.getSchedule],
    [getScheduleSubject_1.openapiGetScheduleSubject, getScheduleSubject_1.getScheduleSubject],
    [getSubjectScore_1.openapiGetSubjectScore, getSubjectScore_1.getSubjectScore],
    // // POST ROUTES
    [postSubject_1.openapiPostSubject, postSubject_1.postSubject],
    [postTeacher_1.openapiPostTeacher, postTeacher_1.postTeacher],
    [postStudent_1.openapiPostStudent, postStudent_1.postStudent],
    [postClass_1.openapiPostClass, postClass_1.postClass],
    [postSchedule_1.openapiPostSchedule, postSchedule_1.postSchedule],
    [postSubjectScore_1.openapiPostSubjectScore, postSubjectScore_1.postSubjectScore],
    // // PUT ROUTES
    [putSubject_1.openapiPutSubject, putSubject_1.putSubject],
    [putTeacher_1.openapiPutTeacher, putTeacher_1.putTeacher],
    [putStudent_1.openapiPutStudent, putStudent_1.putStudent],
    [putClass_1.openapiPutClasses, putClass_1.putClasses],
    // DELETE ROUTES
    // [openapiDeleteSupplier, deleteSupplier],
    [deleteSubject_1.openapiDeleteSubject, deleteSubject_1.deleteSubject],
    [deleteTeacher_1.openapiDeleteTeacher, deleteTeacher_1.deleteTeacher],
    [deleteStudent_1.openapiDeleteStudent, deleteStudent_1.deleteStudent],
    [deleteClass_1.openapiDeleteClasses, deleteClass_1.deleteClasses]
];
const tmpBaseOpenApiRoutes = [];
const tmpBaseRoutes = [];
for (const route of routes) {
    if (!route[1].path.startsWith("/v1/") || !route[0].path.startsWith("/v1/")) {
        // tslint:disable-next-line: no-console
        console.log(route[1]);
        throw new Error("path should start with v1");
    }
    tmpBaseOpenApiRoutes.push(route[0]);
    tmpBaseRoutes.push(route[1]);
}
// seperate the route to openapi and application route, future easier to extend
exports.baseOpenApiRoutes = tmpBaseOpenApiRoutes;
exports.baseRoutes = tmpBaseRoutes;
//# sourceMappingURL=index.js.map