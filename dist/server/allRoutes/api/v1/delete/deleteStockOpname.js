"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteStockOpname = exports.openapiDeleteStockOpname = void 0;
const scripts_1 = require("../../../../../scripts");
const path = "/v1/stockopname/:code";
const method = "DELETE";
const get = [];
const headerParams = scripts_1.commonHeaderParams;
const pathParams = [{
        name: "code",
        required: true,
        swagger: {
            description: "",
            example: "",
        },
        type: "string",
    }];
const responseFormat = {
    items: {
        fields: {
            Code: {
                type: "any",
            },
            Desc: {
                type: "string",
            },
            ID: {
                type: "string",
            },
            LastUpdate: {
                type: "string",
            },
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Get path
    const params = di.validator.processPath(pathParams, ctx.params);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.stockOpnameService.delete(params.code);
};
exports.openapiDeleteStockOpname = {
    get,
    headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.deleteStockOpname = {
    func,
    method,
    path,
};
//# sourceMappingURL=deleteStockOpname.js.map