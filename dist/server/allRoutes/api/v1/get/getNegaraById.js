"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getNegaraById = exports.openapiGetNegaraById = void 0;
const scripts_1 = require("../../../../../scripts");
const path = "/v1/negara/:code";
const method = "GET";
const get = [];
const headerParams = scripts_1.commonHeaderParams;
const pathParams = [{
        name: "code",
        required: true,
        swagger: {
            description: "",
            example: "",
        },
        type: "string",
    }];
const responseFormat = {
    items: {
        fields: {
            Code: {
                type: "any",
            },
            Desc: {
                type: "string",
            },
            ID: {
                type: "string",
            },
            LastUpdate: {
                type: "string",
            },
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Get path
    const params = di.validator.processPath(pathParams, ctx.params);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.negaraService.findById(params.code);
};
exports.openapiGetNegaraById = {
    get,
    headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.getNegaraById = {
    func,
    method,
    path,
};
//# sourceMappingURL=getNegaraById.js.map