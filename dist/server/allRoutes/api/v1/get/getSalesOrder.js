"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSalesOrder = exports.openapiGetSalesOrder = void 0;
const scripts_1 = require("../../../../../scripts");
const appValidator_1 = require("../../../../../lib/appValidator");
const path = "/v1/salesorder";
const method = "GET";
const get = [
    ...appValidator_1.pageableGetParamValidator,
    {
        minLength: 1,
        name: "fields",
        required: false,
        swagger: {
            description: "Fields needed from response",
            example: "Code,Desc,ID,LastUpdate",
        },
        type: "string",
        default: "Code,Desc,ID,LastUpdate",
    }
];
const headerParams = scripts_1.commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            ID: {
                type: "number",
            },
            customer: {
                type: "string",
            },
            Date: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    const query = di.validator.processQuery(get, ctx.request.query);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.salesOrderService.findAll(query);
};
exports.openapiGetSalesOrder = {
    get,
    headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.getSalesOrder = {
    func,
    method,
    path,
};
//# sourceMappingURL=getSalesOrder.js.map