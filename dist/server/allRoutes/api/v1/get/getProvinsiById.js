"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProvinsiById = exports.openapiGetProvinsiById = void 0;
const scripts_1 = require("../../../../../scripts");
const path = "/v1/provinsi/:code";
const method = "GET";
const get = [];
const headerParams = scripts_1.commonHeaderParams;
const pathParams = [{
        name: "code",
        required: true,
        swagger: {
            description: "",
            example: "",
        },
        type: "string",
    }];
const responseFormat = {
    items: {
        fields: {
            ID: {
                type: "any",
            },
            Name: {
                type: "string",
            },
            NegaraId: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Get path
    const params = di.validator.processPath(pathParams, ctx.params);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.provinsiService.findById(params.code);
};
exports.openapiGetProvinsiById = {
    get,
    headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.getProvinsiById = {
    func,
    method,
    path,
};
//# sourceMappingURL=getProvinsiById.js.map