"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.putClasses = exports.openapiPutClasses = void 0;
const scripts_1 = require("../../../../../scripts");
const path = "/v1/classes";
const method = "PUT";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            code: "",
            description: "",
        },
    },
    fields: {
        id: {
            type: "number",
            required: true
        },
        name: {
            type: "string",
            minLength: 1,
            required: true
        },
        homeroomTeacherId: {
            type: "number",
            required: true
        },
        year: {
            type: "number",
            required: true
        }
    },
};
const headerParams = scripts_1.commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            name: {
                type: "any"
            },
            homeroomTeacherId: {
                type: "any"
            },
            year: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.classService.update(requestBody);
};
exports.openapiPutClasses = {
    get,
    headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.putClasses = {
    func,
    method,
    path,
};
//# sourceMappingURL=putClass.js.map