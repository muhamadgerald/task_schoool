"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.putPost = exports.openapiPutPost = void 0;
const scripts_1 = require("../../../../../scripts");
const path = "/v1/posts/";
const method = "PUT";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            code: "",
            description: "",
        },
    },
    fields: {
        code: {
            type: "string",
            minLength: 1,
            maxLength: 3,
            required: true
        },
        author: {
            type: "string",
            minLength: 1,
            required: true
        },
        description: {
            type: "string",
            minLength: 3,
            maxLength: 100,
            required: true
        }
    },
};
const headerParams = scripts_1.commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            Code: {
                type: "any",
            },
            Desc: {
                type: "string",
            },
            ID: {
                type: "string",
            },
            LastUpdate: {
                type: "string",
            },
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.postService.update(requestBody);
};
exports.openapiPutPost = {
    get,
    headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.putPost = {
    func,
    method,
    path,
};
//# sourceMappingURL=putPost.js.map