"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postSalesOrder = exports.openapiPostSalesOrder = void 0;
const path = "/v1/salesorder";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        customer: {
            type: "number",
            required: true
        },
        date: {
            type: "string",
            required: true
        },
        products: {
            type: "array",
            required: true,
            items: {
                type: "object",
                required: true,
                fields: {
                    productId: {
                        type: "number",
                        required: true
                    },
                    quantity: {
                        type: "number",
                        required: true
                    }
                }
            }
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            salesOrder: {
                type: "any"
            },
            date: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.salesOrderService.create(requestBody);
};
exports.openapiPostSalesOrder = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postSalesOrder = {
    func,
    method,
    path,
};
//# sourceMappingURL=postSalesOrder.js.map