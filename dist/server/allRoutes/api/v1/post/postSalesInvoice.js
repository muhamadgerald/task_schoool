"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postSalesInvoice = exports.openapiPostSalesInvoice = void 0;
const path = "/v1/sales-invoice";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        salesOrder: {
            type: "number",
            required: true
        },
        products: {
            type: "array",
            required: true,
            items: {
                type: "object",
                required: true,
                fields: {
                    productId: {
                        type: "number",
                        required: true
                    },
                    pricePerItem: {
                        type: "number",
                        required: true
                    }
                }
            }
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            salesOrder: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.salesInvoiceService.create(requestBody);
};
exports.openapiPostSalesInvoice = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postSalesInvoice = {
    func,
    method,
    path,
};
//# sourceMappingURL=postSalesInvoice.js.map