"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postSubjectScore = exports.openapiPostSubjectScore = void 0;
const path = "/v1/subjectscore";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        classId: {
            type: "number",
            required: true
        },
        subjectId: {
            type: "number",
            required: true
        },
        studentId: {
            type: "number",
            required: true
        },
        term_1: {
            type: "string",
            required: true
        },
        term_2: {
            type: "string",
            required: true
        },
        term_3: {
            type: "string",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            name: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.subjectScoreService.create(requestBody);
};
exports.openapiPostSubjectScore = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postSubjectScore = {
    func,
    method,
    path,
};
//# sourceMappingURL=postSubjectScore.js.map