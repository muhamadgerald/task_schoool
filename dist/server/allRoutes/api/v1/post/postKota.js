"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postKota = exports.openapiPostKota = void 0;
const path = "/v1/kota";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        name: {
            type: "string",
            minLength: 1,
            required: true
        },
        provinsi: {
            type: "number",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            Code: {
                type: "any",
            },
            Desc: {
                type: "string",
            },
            ID: {
                type: "string",
            },
            LastUpdate: {
                type: "string",
            },
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.kotaService.create(requestBody);
};
exports.openapiPostKota = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postKota = {
    func,
    method,
    path,
};
//# sourceMappingURL=postKota.js.map