"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postPurchaseOrderDetail = exports.openapiPostPurchaseOrderDetail = void 0;
const path = "/v1/purchase-order-detail";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        purchaseOrder: {
            type: "number",
            required: true
        },
        product: {
            type: "number",
            required: true
        },
        quantity: {
            type: "number",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            purchaseOrder: {
                type: "number"
            },
            product: {
                type: "number"
            },
            quantity: {
                type: "number"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.purchaseOrderDetailService.create(requestBody);
};
exports.openapiPostPurchaseOrderDetail = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postPurchaseOrderDetail = {
    func,
    method,
    path,
};
//# sourceMappingURL=postPurchaseOrderDetail.js.map