"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postOpname = exports.openapiPostOpname = void 0;
const path = "/v1/stockopname";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        product: {
            type: "number",
            required: true
        },
        stock: {
            type: "number",
            required: true
        },
        date: {
            type: "string",
            required: true
        },
        note: {
            type: "string",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            product: {
                type: "any"
            },
            stock: {
                type: "any"
            },
            note: {
                type: "any"
            },
            date: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.stockOpnameService.create(requestBody);
};
exports.openapiPostOpname = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postOpname = {
    func,
    method,
    path,
};
//# sourceMappingURL=postStockOpname.js.map