"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postSchedule = exports.openapiPostSchedule = void 0;
const path = "/v1/schedule";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        classId: {
            type: "number",
            required: true
        },
        day: {
            type: "string",
            required: true
        },
        subjectId: {
            type: "number",
            required: true
        },
        teacherId: {
            type: "number",
            required: true
        },
        startTime: {
            type: "string",
            required: true
        },
        endTime: {
            type: "string",
            required: true
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            classId: {
                type: "any"
            },
            Day: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.scheduleService.create(requestBody);
};
exports.openapiPostSchedule = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postSchedule = {
    func,
    method,
    path,
};
//# sourceMappingURL=postSchedule.js.map