"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postClass = exports.openapiPostClass = void 0;
const path = "/v1/classes";
const method = "POST";
const get = [];
const body = {
    required: true,
    type: "object",
    swagger: {
        description: "",
        example: {
            author: "",
            description: "",
        },
    },
    fields: {
        name: {
            type: "string",
            required: true
        },
        homeroomTeacherId: {
            type: "number",
            required: true
        },
        year: {
            type: "number",
            required: true
        },
        students: {
            type: "array",
            required: true,
            items: {
                type: "object",
                required: true,
                fields: {
                    studentId: {
                        type: "number",
                        required: true
                    }
                }
            }
        }
    },
};
// const headerParams: ValidatorHeaderParam[] = commonHeaderParams;
const responseFormat = {
    items: {
        fields: {
            id: {
                type: "any"
            },
            name: {
                type: "any"
            },
            homeroomTeacherId: {
                type: "any"
            },
            year: {
                type: "any"
            }
        },
        type: "object",
    },
    type: "array",
};
const successResponseFormat = {
    fields: {
        data: responseFormat,
    },
    type: "object",
};
const func = async (ctx) => {
    const di = ctx.state.di;
    // Validate query params
    di.validator.processQuery(get, ctx.request.query);
    // Validate body
    const requestBody = di.validator.processBody(body, ctx.request.body);
    // If you need to validate the headers, uncomment this
    // const headers = di.validator.processHeader<ICommonHeaderParamsProcessed>(headerParams, ctx.request.headers);
    ctx.status = 200;
    ctx.body = await di.classService.create(requestBody);
};
exports.openapiPostClass = {
    get,
    // headerParams,
    method,
    path,
    successResponseFormat,
    tags: ["masterdata"],
};
exports.postClass = {
    func,
    method,
    path,
};
//# sourceMappingURL=postClass.js.map